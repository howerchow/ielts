#separator:tab
#html:false

verdict	n. 裁决
After ten hours of deliberation, the jury returned a verdict of 'not guilty'. 经过十小时的商议，陪审团宣告了“无罪”的裁决

circumscribe	v. 限制 His activities have been severely circumscribed since his illness.
predecessor	n. 前任 The new president reversed many of the policies of his predecessor. 新任总统彻底改变了其前任的许多政策。
vicissitude	n. 变迁兴衰  The city has been through many vicissitudes. 这个城市经历了多次兴衰。
assortment	n. 各种各样 a wide assortment of gifts to choose from 各式各样可供挑选的礼品
expletive	n. 补充物 a. 补充的
forgo	v. 放弃 No one was prepared to forgo their lunch hour to attend the meeting.
pedigree	n. 动物的血统 dogs with good pedigrees 纯种的狗
a policy of appeasement...	绥靖政策
apparatus	n. 设备 a piece of laboratory apparatus
shudder	v. 打颤  Alone in the car, she shuddered with fear. 她一个人待在车里，害怕得直哆嗦。
empirical	a. 以经验为依据的
perverse	a. 任性的，无理取闹的 Do you really mean that or are you just being deliberately perverse? 你是真要那样，还是故意作对？
inaugurate	v. 1. ~ sb (as sth)为（某人）举行就职典礼  He will be inaugurated (as) President in January. 他将于一月份就任总统。 2.  为…举行落成仪式 The new theatre was inaugurated by the mayor. 新落成的剧院由市长主持了开幕典礼。 3.开创 The moon landing inaugurated a new era in space exploration. 登陆月球开创了太空探索的新纪元。
suppress	v. 被压制  The rebellion was brutally suppressed. 起义遭到了残酷的镇压。
poise	v. 镇静   He was poising himself to launch a final attack.
retrenchment	n. 紧缩 Retrenchment not expansion is now the policy of most companies. 削减而不扩张是现在大多数公司的方针。
elusive	a. 难以抓住的
consignment	n.托运物 the first consignment of food
hostile	a. 不友善的
unprecedented	a. 前所未有的 We are now witnessing an unprecedented increase in violent crime. 我们现在看到暴力犯罪空前增多。

exacerbate	v. 加剧，加重 The symptoms may be exacerbated by certain drugs. 这些症状可能会因为某些药物而加重。
abashed	a. 羞愧的 His boss's criticism left him feeling rather abashed.
abate	v. 减少 The storm showed no signs of abating.
abdicate	v. 放弃权力，退位 He abdicated in favour of his son. 他把王位让给了儿子。
abduct	v. 劫持，绑架 He was abducted by four gunmen.
abhorrent	a. 可恨的 Racism is abhorrent to a civilized society.
abiding	a. 永久的 As we know, study is a abiding thing.
ablaze	a. 燃烧的 The whole building was soon ablaze. 整栋大楼很快就熊熊燃烧起来。 Cars and buses were set ablaze during the riot. 暴乱中许多轿车和公共汽车被纵火焚烧。 a. 闪耀的  The chamber was ablaze with light. 会议厅里灯火辉煌。
aboriginal	n. a.土著  the aboriginal peoples of Canada
abstain	v. 戒绝  Because my cholesterol is high, my doctor told me to abstain from eating fat.
acclaim	v.喝彩，称赞  The work was acclaimed as a masterpiece. 该作品被誉为杰作。
accomplice	n. 从犯 He was working alone and didn’t have a accomplice.
accost	vt. 向人搭话 She was accosted in the street by a complete stranger. 在街上，一个完全陌生的人贸然走到她跟前搭讪。
acrimonious	a. 言辞尖刻，激烈的  There followed an acrimonious debate. 接着是一场唇枪舌剑的争辩。
ordeal	n. 严酷的考验
oust	v. 革职，罢免  ~ sb (from sth/as sth) He was ousted as chairman. 他的主席职务被革除了。
overbearing	a. 专横的 My husband can be quite overbearing with our son. 我丈夫有时对儿子很专横

panacea	n. 万灵药
devious	a.曲折的  a devious explanation of  对。。很绕的解释
devise	vt. 计划，发明 A new system has been devised to control traffic in the city.

devour	vt. 吞食 He devoured three sandwiches. Flames devoured the house.
discharge	n. 排出物
discourse on sth.	n. 演讲=disquisition
discreet	a. 谨慎的 prudent, cautious

frenzy	n. 狂热 an outbreak of patriotic frenzy 爱国狂热的迸发
fret	v. 苦恼  Fretting about it won't help. 苦恼于事无补
frigid	a. 严寒的 frigid temperatures
fulminate	v. 猛烈抨击 They all fulminated against the new curriculum.
futile	a. 徒劳的 It would be futile to protest.
gabble on about	口齿不清地说
gainsay	vt. 否认，deny
gauche	a.   不善交际的 We're all a bit gauche when we're young.

genial	a. 和蔼的 Bob was always genial and welcoming. 鲍勃总是那么友善热情
gentility	n. 教养 sb brougt up with gentility 家教很好的人

proclaim	v. 宣布，申明  The president proclaimed a state of emergency. 总统宣布了紧急状态。
prod	v. 戳 She prodded him in the ribs to wake him up.
prodigal	a. 浪费的  He has been prodigal of the money left by his parents.
prodigious	a. 巨大的 a prodigious achievement/memory/talent  惊人的成就 / 记忆力 / 才华 Laser discs can store prodigious amounts of information. 激光磁盘能够贮存大量信息。
profane	a. 亵渎的  profane language 亵渎上帝的语言
profuse	a. 大量的 He was bleeding profusely. 他正在大出血。
prop n.	v. 支持 n. 道具 He is responsible for all the stage props and lighting. 他负责全部舞台道具和灯光。
protract	v. 拖延（贬义） The war was protracted for four years. 战争拖延了四年 a protracted war 持久战
provident	a. 有远见的 A provident father plans for his children's education. 有远见的父亲为自己孩子的教育做长远打算。
provoke	v. 激怒 provoke the bees
irrevocable	a. 不能取消的
jerk	v. 抽搐 My muscle jerked.
jettison	v. 抛弃 The Government seems to have jettisoned the plan. 政府似乎已经放弃了这个计划。 The crew jettisoned excess fuel and made an emergency landing. 机组人员丢弃了多余燃料，紧急着陆。
ken	n.知识范围  The subject matter was so technical as to be beyond the ken of the average layman. 该主题专业性太强，一般的外行人很难理解。
labyrinth	n. 迷宫，错综复杂的事 We lost our way in the labyrinth of streets. 我们在迷宫式的街道上迷了路。

languid	a. 懒散的，慢悠悠的  a languid afternoon in the sun 阳光下一个懒洋洋的下午
languish	v. 变衰弱 Without the founder's drive and direction, the company gradually languished. 没有了创始人的斗志与指引，公司逐渐走向没落。
lash	v. 鞭打 The horse is lashed.

laurel	n. 月桂树
leash	n. 牵狗绳 All dogs must be kept on a leash in public places.
lethal	a. 致命的
levy	v. 征税 levy high taxes on sb.
listless	a. 没精打采的 He was listless and pale and wouldn't eat much. 他无精打采，脸色苍白，食欲不振。
lithe	a. 柔软的，易弯的 a lithe young gymnast 身体柔软的年轻体操运动员
loathe	v. 极其厌恶，通常比hate语气还强 I loathe modern art.
longitude	n. 经度 xx city is at longitude 21 ° east. 这个城市位于东经21度。
loot	v. 掠夺 More than 20 shops were looted.
lurk	v. 潜藏  The murderers lurked behind the trees. 谋杀者埋伏在树后。
luster	n. 光影 The chair has a metallic luster.
luxuriant	a. 繁茂的 luxuriant vegetation 茂密的草木 thick, luxuriant hair 浓密的头发
maim	v. 使残废 He was maimed in a car accident.
malcontent	n. 不满者，愤青
malevolent	a. 恶意的
malice	n. 恶意   He sent the letter out of malice. 他出于恶意寄出了这封信。 She is entirely without malice. 她完全没有恶意。
malign	v. 公开诋毁 She feels she has been much maligned by the press. 她觉得她遭到了新闻界的恣意诽谤。
malleable	a. 有延展性的  Silver is the most malleable of all metals. 银是延展性最好的金属。
mandate	v. 命令  The law mandates that imported goods be identified as such. 法律规定进口货物必须如实标明。
mangle	v. 撕裂，毁坏 His hand was mangled in the machine. 他的手被机器绞烂了。 mangled bodies 面目全非的尸体
manifold	a. 多样的 The possibilities were manifold.
manure	n. 肥料 She should put some manure on her land.
mass	n. 质量, 团，块 a black mass 黑黑的一片
meander	v. 蜿蜒而流 The river meanders through the mountain to the east.
meddle	v. 干预 meddle in the affairs of others
mediate	v. 调停 ~ (in sth) | ~ (between A and B) The Secretary-General was asked to mediate in the dispute. 有人请秘书长来调解这次纷争。 An independent body was brought in to mediate between staff and management. 由一个独立机构介入，在劳资之间进行调解。
menace	v. 威胁  The forests are being menaced by major development projects. 大型开发项目正在危及森林。
incredulity	n. 怀疑 The announcement has been met with incredulity. 通告受到了人们的质疑。
incriminate	v. 控告 He was incriminated of murder.
incur	v. 招致 He incurred the wrath of the authorities in speaking out against government injustices.   I'll be happy to reimburse you for any expenses you might have incurred. 我将很乐意赔偿您所发生的任何费用。
indefatigable	a. 不疲倦的（褒义） an indefatigable defender of human rights
indemnity	n. 赔偿 indemnity insurance 赔偿保险
indigenous	a. 土著的 indigenous people 本地人
indigent	a. 十分贫穷的
indignant	a. 愤慨的  ~ (at/about sth) | ~ (that…) She was very indignant at the way she had been treated.
indolent	a. 懒惰的
indomitable	a. 勇敢坚定的 has an indomitable spirit.
ineligible	a. 没有资格的 ~ (for sth/to do sth) ineligible for financial assistance 无资格得到财政援助 ineligible to vote 无表决资格
inept	a. 无能的 He was inept and lacked the intelligence to govern.
inert	a. 不动的 He covered the inert body with a blanket. 他用毯子把那具一动不动的尸体盖上。

infallible	a. 不会出错的 Doctors are not infallible. 医生并非永不犯错。
infer	v. 推知 Much of the meaning must be inferred from the context. Are you inferring (that) I'm not capable of doing the job?
inflict	v.使遭受 The economic depression has inflicted hundreds of millions of loss in Asia.
influx	n. 流入  problems caused by the influx of refugees. 大批难民涌入产生的问题
infringe	v. 侵犯，违反  your book infringes my copy right
inherent	a. 固有的 Violence is inherent in our society. 在我们的社会中暴力是难免的。 an inherent weakness in the design of the machine 机器设计中的内在缺陷
injurious	a. 有伤害的 Reading while riding in a car is injurious to one's eyes. 在汽车上看书容易损害眼睛。
innate	a. 天生的  Knowledge is acquired, not innate.

inordinate	a. 无节制的，过度的  They spend an inordinate amount of time talking
insinuate	v. 暗示，影射 The article insinuated that he was having an affair with his friend's wife. 文章含沙射影地点出他和朋友的妻子有染。
insolent	a. 傲慢的，无礼的 Her insolence cost her her job. 她的蛮横态度使她丢了工作。
ductile	a. 可延展的   This metal is workable and ductile. 这种金属易加工，延展性也好。 Wax is ductile when heated. 蜡在受热时是可塑的。
dwindle	v. 减少  Membership of the club has dwindled from 70 to 20.
economize	v. 节俭  When they bought a house, the family had to economize to pay their mortgage.
ecstasy	n. 狂喜 My father was in ecstasy when I won my scholarship. 我获得奖学金时父亲欣喜若狂。
edify	v. 陶冶，教化  A trip to the art museum edified the tourists and helped them understand the local culture better.
efface	v. 消除   Time alone will efface those unpleasant memories.
effuse	v. 流出

elicit	v. 得出，引出 provoke
elucidate	v. 阐明 elucidate one’s idea
elude	v. 躲避 the gangster eluded the police
emaciate	v. 使。。瘦弱 In the light of a single candle, she can see his emaciated face.
embellish	v. 装饰 She embellished the shirt collar with lace
embody	v. 体现，包含 The monitor embodied his idea in a short speech
impartial	a. 公正的，无偏见的 Scientists, of course, can be expected to be impartial and disinterested
appraisal	n. 评价 staff/performance appraisal 员工 / 工作表现评估
impassioned	a. 热烈的 He made an impassioned appeal for peace.
impassive	a. 冷漠的，面无表情的 impassive face
impeccable	a. 无瑕的
impel	v. 驱使 be impelled by 被。。驱动
imperative	a. 急需的 The man is dying, an immediate operation is imperative.
impertinent	a. 无关的 Don’t talk anything impertinent with the main issue.
impervious	a. 不能渗透的 impervious to water 防水的


plausible	a. 合理的，貌似合理的
constituent	n. 成分，组成部分 Caffeine is the active constituent of drinks such as tea and coffee.
viscous	a. （液体）粘稠的 viscous blood
impetus	n. 动力 His articles provided the main impetus for change. 他的那些文章是促进变革的主要推动力。
amass	v. 积累 He amassed a fortune from silver mining. 他靠开采银矿积累了一笔财富。
sterilization	n. 灭菌，消毒
stray cat	流浪猫

dispense with..	免除，省掉
be poised to do	准备就绪做
mundane	a. 单调的，平凡的 a mundane task/job 平凡的任务 / 职业 I lead a pretty mundane existence. 我过着相当平淡的生活。
camouflage	v. 伪装 a camouflage jacket 迷彩夹克 The soldiers camouflaged themselves with leaves.
encapsulate	v. 概括 A Wall Street Journal editorial encapsulated the views of many conservatives. 《华尔街日报》的一篇社论概述了很多保守派人士的观点。
culprit	n. 罪魁祸首 The police quickly identified the real culprits. 警方很快查出了真正的罪犯。 About 10% of Japanese teenagers are overweight. Nutritionists say the main culprit is increasing reliance on Western fast food.
degradation	n. 堕落 environmental degradation 环境恶化
repatriate	v. 遣送回国；将利润带回本国 About 300 French hostages are to be repatriated. 大约300名法国人质即将被遣送回国。 Foreign investors are to be allowed to repatriate profits over one billion rupees. 外国投资者将被允许将利润额超过10亿卢比的部分调回国内。
exert	v. 发挥，行使 He exerted all his authority to make them accept the plan. 他利用他的所有权力让他们接受这个计划。 ~ yourself 努力；竭力  In order to be successful he would have to exert himself.
tensile	a. 张力的，可伸长的

furnace	n. 熔炉 It's like a furnace (= very hot) in here! 这里热得像火炉！
cue	v. 暗示，信号 Can you cue me when you want me to begin speaking? 你要我开始讲话时能给我暗示一下吗？
fieldwork	n. 实地考察

at the outset, at the beginning	在开始

deride	v. 嘲笑    [often passive] ~ sb/sth (as sth) His views were derided as old-fashioned.
desolate	a. 荒凉的 a bleak and desolate landscape
despoil	v. 抢劫 The army despoil the city of all its treasure.

deterrent	n. 威慑物 Hopefully his punishment will act as a deterrent to others. 对他的惩罚但愿能起到杀一儆百的作用。 the country's nuclear deterrents 这个国家核武器的威慑力
detract	v. 贬低 His bad manners detract from his good character.
detriment	n. 损害 He was engrossed in his job to the detriment of his health. 他全身心地投入工作结果损害了他的健康。
detrimental	a. 有害的 be detrimental to your health

deviate from sth	偏离。。
declaim	v. 慷慨激昂地朗诵  She declaimed the famous opening speech of the play. 她慷慨激昂地朗诵了这出戏中著名的开场白。
decree	n. 法令，规定  to issue/sign a decree

deference	n. 敬意 The flags were lowered out of deference to the bereaved family. 降旗是出于对死者家属的尊重。
deferential	a. 敬意的 They like five-star hotels and deferential treatment.
deflect	v. 使偏离 All attempts to deflect attention from his private life have failed.
defraud	v. 欺诈某人 They were accused of defrauding the company of $14 000. 他们被控诈骗该公司14 000元
deft	a. 灵巧的，熟练的 One of the waiting servants deftly caught him as he fell. 他快要跌倒时，其中一个在旁服侍的仆人眼疾手快地扶住了他。
defy the law	违抗法律
degenerate	v. 恶化 The march degenerated into a riot. 示威游行变成了暴动。 Her health degenerated quickly. 她的健康状况迅速恶化。
denote	v. 象征 A very high temperature often denotes a serious illness. 表示 In this example ‘X’ denotes the time taken and ‘Y’ denotes the distance covered.
denounce	v. 谴责 She publicly denounced the government's handling of the crisis. 告发 They were denounced as spies. 有人检举他们，说他们是间谍。
deplete	vt. 耗尽 substances that deplete the ozone layer 大量消耗臭氧层的物质
creep	v. 爬，蹑手蹑脚
crevice	n. 裂缝
crude	a. 油或其他天然物质，天然的，自然的 Crude oil   原油
crumple	v. 压皱 crumple the letter 把信捏成一团
culmination	n. 顶点 at the culmination of her career
curb	v. 抑制 curb inflation
curtail	v. 缩减 Spending on books has been severely curtailed. 购书开支已被大大削减。
dab	v. 轻拍
decadence	n. 衰落，颓废 The country will inevitably now plunge headlong into decadence.  整个国家会不可避免地一头陷入衰退中。
contemplate	v. 考虑 I have never contemplated living abroad.   深思熟虑；沉思 contemplate your future
contemptible	a. 可鄙的 He never believed those contemptible rumours.
contemptuous	a. 藐视的~ (of sb/sth) He was contemptuous of everything I did. 他对我所做的一切都不屑一顾。
contend	v. 争斗~ (for sth) Three armed groups were contending for power.
contingent	a. 依情况而定的 Managers need to make rewards contingent on performance.
contravene	v. 违反 The company was found guilty of contravening safety regulations. 那家公司被判违反了安全条例。
contrite	a. 悔悟的 She was instantly contrite. 'Oh, I am sorry! You must forgive me.'
contrive	v. 设法做到 After much difficulty, he contrived to stop drinking!
controvert	v. 反驳 This is a fact that cannot be controverted.
convene	v. 召集，召开 A Board of Inquiry was convened immediately after the accident. 事故后调查委员会立即召开了会议。
corpulent	a. 发福的，肥胖的 Her father is too corpulent to play handball.
cosmopolitan	a. 全世界的  n.四海为家的人
counsel	n. 劝告 refuse to listen to one’s counsel
counterpart	n.   职位（或作用）相当的人； The Foreign Minister held talks with his Chinese counterpart. 外交部长与中国外交部长举行了会谈。
clamor	n. 喧闹 He ignored the clamor of the crowd.
clutter	n. 混乱 v. 凌乱地塞满 等于 clutter up The vehicles cluttered up the car park.
coddle	v. 溺爱 She coddled her youngest son madly.
cohesion	n. 附着力，凝聚力 social cohesion
collateral	a. 附带的 The government denied that there had been any collateral damage.  政府否认空袭期间有任何附带性的破坏（即对平民或建筑物的损害）。 n. 抵押品 Many people use personal assets as collateral for small business loans.
commonplace	n. 常见的事 a. 普遍的 Computers are now commonplace in primary classrooms. 计算机如今在小学教室里很普遍。

complement	n. 补充物，补足 The green wallpaper is the perfect complement to the old pine of the dresser.
comply with	服从，。
compulsive	a.   难以制止的，  无法控制行为的 a compulsive drinker/gambler/liar
concede	v. 让步~ sth (to sb) | ~ sb sth The President was obliged to concede power to the army. 总统被迫把权力让给军队。

concerted	a. 同心协力的 We need to make a concerted effort.
concise	a. 简明的 a concise summary 简明扼要的总结
concur	v. 同时发生的，同意 concur with sb
condiment	n. 调味品 Mustard and ketchup are condiments.
confound	v. 使糊涂，迷惑某人 The sudden rise in share prices has confounded economists.
congenial	a. 意气相投的 a congenial colleague 意气相投的同事
congregate	v. 聚集 The crowds congregated in the town square to hear the mayor speak.

consort	v. 厮混 ~ with sb He regularly consorted with known drug-dealers. n. 配偶 the prince consort (= the queen's husband) 亲王（女王的丈夫）
conspicuous	a. 引人注目的  The ads are posted in a conspicuous place.
constitute	vt. 组成 be constituted of , be composed of, consist of, comprise xxx Female workers constitute the majority of the labour force.
constrain sb to do sth	v. 强迫 compel
equate xx with xx	将xx等同于xx
equilibrium	n. 平衡状态  Stocks seesawed ever lower until prices found some new level of equilibrium. 股票在震荡中进一步下跌，直到股价达到某个新的平衡点。
equitable	a. 公平的，公正的 50 dollars is an equitable price for this lamp.
equivocal	a. 模凌两可的 She gave an equivocal answer, typical of a politician.

erratic	a. 古怪的，不稳定的 erratic moods
eschew	v. 避开，远离 eschew alcohol
escort sb	v. 护送
ethereal	a. 超凡的 ethereal music 优雅的音乐 her ethereal beauty 她飘逸的美
etiquette	n. 礼节 This goes against social etiquette.
evasion	n. 逃避 evasion of questions
evoke	v. 唤起 His case is unlikely to evoke public sympathy. 他的情况不大可能引起公众的同情。 The music evoked memories of her youth. 这乐曲勾起了她对青年时代的回忆。
exalt	v. 称赞 This book exalts her as both mother and muse.
exalted	a. 地位高的，显赫的 She was the only woman to rise to such an exalted position.
exasperate sb.	v. 激怒xxx She was becoming exasperated with all the questions they were asking.
excavate	v. 挖掘 excavate a hole
excel in sth	擅长。。
impotence	n. 无效，无力，虚弱 a sense of impotence 无能为力的感觉
impromptu	a. 临时的，即兴的 an impromptu performance
improvident	a. 浪费的 lived improvident lives
imprudent	a. 轻率的，不谨慎的
impudent	a. 粗鲁的 an impudent young fellow 莽撞的年轻人
impulsive	a. 冲动的 an impulsive decision
impute	impute sth to sb/sth (formal) （常指不公正地把责任等）归咎于  It is grossly unfair to impute blame to the United Nations. 把责任归咎于联合国极其不公。
incapacitate	v. 使失去正常生活(或工作)能力 A serious fall incapacitated the 68-year-old congressman. 一次严重跌伤使这位 68 岁的国会议员丧失了行动能力。
incense	v. 激怒 The decision incensed the workforce. 这个决定激怒了劳工大众。
incessant	a. 不断的 incessant rain 阴雨连绵 to talk incessantly 滔滔不绝地谈话 incessant meetings 接二连三的会议
incipient	a. 初期的 signs of incipient unrest 动乱的初期迹象
inconceivable	a. 不可思议的 It's inconceivable that she should do something like that.
incongruous	a. 不协调的 Such traditional methods seem incongruous in our technical age.
emend	v. 校订 The manuscript was emended by the author.
eminent	a. 杰出的 the eminent poet
emphatic	a. 显著的，强调的，有力的
empower sb to do sth	v. 授权某人做某事 The courts were empowered to impose the death sentence for certain crimes.
emulate	v. 仿效，努力赶上 She hopes to emulates her sister’s sporting achievements.
enact	v. 制定法律 legislation enacted by parliament 扮演 scenes enacted by local residents
encompass	v. 包围，环绕 The job encompasses a wide range of responsibilities. 这项工作涉及的职责范围很广。 The group encompasses all ages. 这个小组各种年龄的人都有。
encroach on sth	v. 侵占xx He never allows work to encroach upon his family life. 他从不让工作扰乱他的家庭生活。
encumber	v. 阻碍 The police operation was encumbered by crowds of reporters.
endeavor to do	v. 努力做某事
enervate	v. 使衰弱，没力气  Warm winds make many people feel enervated and depressed.
engender	v. 产生 The issue engendered controversy. 这个问题引起了争论。
engross	v. 使全神贯注 Tony didn't notice because he was too engrossed in his work.
engulf	v. 吞没 Huge waves engulfed the small boat.
enlighten	vt. 启发xxx It was a very enlightening interview. 那次面谈让人很受启发。
enlist	v. 征召，招募 They both enlisted in 1915. 他俩都是1915年入伍的。
enrage	v. 激怒 enrage his supervisor
enrapture	v. 使狂喜，出神  We are enraptured by the view of the mountains.
entail	vt. 涉及 The job entails a lot of hard work.
entangle	v. 使纠缠 be entangled in sth.
entice sb into doing sth	v. 诱惑  Advertisements are designed to entice people into spending money. Retailers have tried almost everything to entice shoppers through their doors
enunciate	v. 阐明 He enunciated his vision of the future.
blunder	v. 犯大错  I really blundered.
boon	n. 非常有用的东西 The new software will prove a boon to Internet users. 这种新软件将会对互联网用户大有益处。
brawl	v. 争吵 brawl about sth.
breach	n. 破裂 They escaped through a breach in the wire fence. 他们从铁丝网上的一个缺口逃走了。
brim	n. 边 a wide-brimmed hat 宽檐帽

burnish	v. 磨光 burnish the brass plates
byword	n. 代名词  A region that had become a byword for violence and degeneracy.
calamity	n. 灾难，祸患 He described drugs as the greatest calamity of the age.
canny	a. 精明的 a canny politician 老谋深算的政治家
captivate	v. 迷惑 captivate sb.
exceptional	a. 卓越的(能力） At the age of five he showed exceptional talent as a musician.
excerpt	n..摘录，节选 an excerpt from Tchaikovsky's Nutcracker
exemplary	a. 模范的 Her behaviour was exemplary.
exhume	v. 挖出，出土 When the police exhume the corpse, they discover trace of poison in it.
exile	v. 流放  The king was exiled
exodus	n. 大批离去  The refugees made an exodus to a safe place.
exorbitant	a. 过分的，过度的 pay such an exorbitant price for sth.  Exorbitant housing price has become a problem.
expedition	n. 旅行，远征  The explorers started in a yearlong expedition down the nile.

expound	v. 解释  She expounded on the government's new policies.

extant	a. 现存的 extant remains of the ancient wall 尚存的古城墙遗迹
extenuate	v. 使。。显得轻微 Your money can not help to extenuate your crime.
extol	vt. 颂扬 exalt She was extolled as a genius.
reconcile	v. 和好 be reconciled with sb. He has recently been reconciled with his wife. 他最近已经和妻子和好了。  v. 调和 It was hard to reconcile his career ambitions with the needs of his children. 他很难兼顾事业上的抱负和孩子们的需要。
pawn	v. 当  I redeemed the watch that I pawned at the pawn shop.
rehabilitate	v. 恢复  Many factories are rehabilitated.
reign	v. 统治 reign the nation
reiterate	v. 重申 He reiterated his opposition.
relapse	v. 复发 Her disease relapsed.
relentless	a. 不停的  Her relentless pursuit of perfection. 她对完美的不懈追求。
relinquish	v. （不情愿）放弃  She relinquished possession of the house to her sister. 她将房子让给了她的妹妹。
renounce	v. 正式宣称放弃  to renounce a claim/title/privilege/right 宣布放弃要求 / 头衔 / 特权 / 权利
howl	v. 咆哮 A dog suddenly howled.
hurl	v. 猛投 He hurled a brick through the window. 他往窗户里扔了块砖。 Rioters hurled a brick through the car's windscreen. 暴徒把一块砖猛地扔向汽车，砸破了挡风玻璃
ignominious	a. 可耻的 an ignominious defeat 可耻的失败
illegible	a. 难辨认的 Parts of the document are faded and illegible.
illicit	a. 违法的 illicit drugs 违禁药物
illustrious	a. 辉煌的，著名的 the most illustrious scientists of the century
imbibe	v. 饮，喝 inside a lot of wine
imminent	a. 即将来临的 the imminent storm Energy saving has become an imminent task.
impale	v. 刺穿  She impaled a lump of meat on her fork. 她用叉子戳起一块肉。 He had fallen and been impaled on some iron railings. 他摔下去，穿在了铁栏杆上
impart	v. 传递 impart knowledge to sb.
prudent	a. 谨慎的 a prudent businessman 精明的商人
puddle	n. 小水洼
pungent	a. 辛辣的 the pungent smell of burning rubber
purge	v. 消除 purge away his sins
quaint	a. 古色古香的 Boppard is a small, quaint town with narrow streets. 博帕德是一个有着许多深巷窄道、古香古色的小镇。

quiver	v. 颤动 His voice quivered with rage. 他气得声音发抖。
rage	n. 激怒 His face was dark with rage. 他气得面色铁青。
rapacious	a. 贪婪的，强夺的 Rapacious developers soon bought up the land. 不久贪婪的开发商就把这块地全部买下了。
rapture	n. 狂喜 The last minute goal sent the fans into raptures.
ratify	v. 批准 The treaty was ratified by all the member states. 这个条约得到了所有成员国的批准。
ravage	v. 掠夺，破坏 For two decades the country has been ravaged by civil war and foreign intervention.
ravenous	a. 贪婪的，极饿的
ravish	v. 使陶醉，狂喜 I’m ravished by her beauty.
displace	v. 取代 displace sth. = replace sth.
dissemble	v. 隐藏，伪装 disguise
dissipate	v. 驱散 The fog is dissipating.
distressed	a. 痛苦的 I feel very alone and distressed about my problem.
divulge	vt. 宣布，泄露 divulge the news
domesticate	vt. 驯养，教化 domesticate the dog
dormant	adj. 休眠的 dormant volcano

thrust	vt. 力推 push  n. 飞机或火箭的助推力 It provides the thrust that makes the craft move forward. 它提供了飞机前进所需的推力。
tint	vt. 上色
torment	n. 痛苦 She suffered years of mental torment after her son's death. 儿子去世后，她多年悲痛欲绝。
torpid	a. 迟钝的，不活泼的
transcend	vt. 超越 Science transcends national borders, but literature often encounters the barrier of language.
transitory	adj. 短暂的 the transitory nature of his happiness 他的幸福昙花一现
transpire	vt. 泄露 It was transpired that the king was already dead.
traverse	vt. 横渡 The railway traverses The country.  这条铁路横贯全国。 We traverse the desert by truck. 我们乘卡车横穿沙漠。
tremor	n. 颤抖 There was a slight tremor in his voice.
trepidation	n. 惊恐，战栗 The threat of an epidemic caused great alarm and trepidation. 流行病猖獗因而人心惶惶。
turmoil	n. 骚动，混乱 the political turmoil in South Africa 南非的政治动荡 Her marriage was in turmoil.  她的婚姻一团糟。
haggle	vi. 争论 haggle about xx
hail	n. 冰雹
hamper	vt. 妨碍 The bad weather hampered rescue operations. 恶劣的天气阻碍了救援工作的进行。
haphazard	a. 偶然的，随便的
hardy	a. 强壮的 hardy and independent people
harry	v. 掠夺，折磨
hasten	v. 催促 The treatment she received may, in fact, have hastened her death.
haughty	a. 傲慢的 He spoke in a haughty tone.
haul	vt. 拖拽，托运 A crane had to be used to haul the car out of the stream. 只好用了起重机，才将轿车从河里拖出来。
haven	n. 港口，避难所 The hire, is a haven of peace and tranquility.
headway	n. 进展  Police were making little headway in the investigation.
hearsay	n. 谣传，风闻 We can't make a decision based on hearsay and guesswork.
heave	vt. 用力举起 They heaved the body overboard. 他们使劲把尸体从船上抛入水中
hectic	a. 忙碌的 lead a hectic life 生活十分忙碌
heedless	a. 不留心的 She is heedless of the noises outside the window. 她没有注意到窗外的嘈杂声
heredity	n. 遗传 Heredity is not a factor in causing the cancer. 这种癌症和遗传无关。 Is this disease hereditary?  这种病遗传吗?
hinge	vi. 依。。。而定 His success hinges on how well he does at the interview.
hoarse	a. 嘶哑的
hoax	v. 愚弄 n. play hoaxes
hoist	v. 升起 n. 吊车  He was hoisted up to the top by a hoist
holocaust	n. 大屠杀
homage	n. 敬意    pay homage to sb/sth
homicide	n. 杀人，等于murder  The police arrived at the scene of the homicide. 警方赶到了凶杀现场
homogenous	a. 同类的，相似的 The population of the small town was homogenous, mostly merchants and laborers.
cardinal	a. 首要的，基本的  As a salesman, your cardinal rule is to do everything you can to satisfy a customer. 作为推销员,你的首要任务是竭尽全力让顾客满意
carnage	n. 大屠杀，残杀
caustic	a. 有腐蚀性的，讽刺的 this caustic chemical
cavern	n. 大山洞，大洞穴
celestial	a. 天上的  celestial bodies (= the sun, moon, stars, etc.) 天体
censure	n. 责难 receive a censure from xx
chafe	vt. 摩擦 The collar was too tight and chafed her neck.
chide	vt. 斥责 chide sb. for doing sth.
acute	a. 敏锐的，急性的
be adept at doing sth	擅长。。
adherent	a. 依附的 n. 信奉者 loyal adherent
adhesive	a. 粘着的 adhesive tape 胶布
adjourn	vt. 延期 The meeting was adjourned until 4 o’clock.
adroit	a. 机巧的 be adroit with his hands
affinity	n. 类似处，密切关系，吸引力 There is a close affinity between apes and monkeys There is a close affinity between Italian and Spanish.
affirm	vt. 断言，证实xx I can affirm that no one will lose their job. 我可以肯定，谁都不会丢掉工作。
afflict	vt. 使痛苦，折磨=torture Financial difficulties afflicted the simons.  A fatal cancer is afflicting her.
affluence	n. 富足 For them, affluence was bought at the price of less freedom in their work environment.
affluent	a. 富裕的 a very affluent neighbourhood
aggravate	vt. 恶化 aggregate one’s financial difficulties
aggregate	n. 总数 They won 4 – 2 on aggregate.  他们以总分4:2获胜。
aggression	n. 侵犯 War is a very serious form of aggression.
agitate	vt. 煽动  agitate the dog
legislate	vi. 立法 The government will legislate against discrimination in the workplace.
loath	a. 不愿意的 ~ to do sth He was loath to admit his mistake. 他不愿承认自己的错误。
malady	n. 严重问题  Violent crime is only one of the maladies afflicting modern society.
propagate	v. 繁殖，宣传
propel	vt. 推进，促进 propel the ball forward 带球向前
prosecute	v. 起诉，检举 The company was prosecuted for breaching the Health and Safety Act.  这家公司被控违反《卫生安全条例》。 Trespassers will be prosecuted.  闲人莫入，违者必究。 The police decided not to prosecute.  警方决定不予起诉。
be fraught with	=be full of 充满
frowzy	a. 不整洁的，臭的filthy
gallant	a. 英勇的 gallant soldiers
generalize from sth	从xxx归纳总结
jumble	vt. 混杂    [usually passive] ~ sth (together/up) Books, shoes and clothes were jumbled together on the floor.
kindle	vt. 燃起 to kindle a fire/flame 点火；点燃火焰
laudable	a. 值得赞美的 a laudable aim/attempt 值得称赞的志向 / 尝试
abridge	vt. 缩短，删节
abstinence	n. 禁欲 Abstinence is the most effective strategy to prevent HPV infection.
accentuate	vt. 重读，强调
discern	vt. 辨明 I can’t discern the difference between the twins.
unanimous	a. 意见一致的 My friends and i made a unanimous decision to order pizza.
undaunted	a. 顽强的，百折不挠的（褒义） He seemed undaunted by all the opposition to his idea.
uprising	n. 叛乱 These uprisings come from desperation and a vista of a future without hope. 发生这些暴动是因为人们被逼上了绝路，未来看不到一点儿希望。
valiant	a. 英勇的 valiant warriors 勇敢的武士
vanquish	vt. 征服  vanquished many foes
variant	n. 变种 This game is a variant of baseball. 这种运动是由棒球演变而来的。
varnish	n. 清漆 The varnish takes a few hours to harden. 清漆需要几个小时才能干透。 v. 上漆 Josie was sitting at her desk, varnishing her nails. 乔西坐在书桌旁，涂着指甲油。
vehement	a. 猛烈的  He had been vehement in his opposition to the idea. 他一直强烈反对这一主张。
venerate	vt. 崇拜 My father venerated General Eisenhower. 我父亲十分敬仰艾森豪威尔将军。
ventilation	n. 通风 Make sure that there is adequate ventilation in the room before using the paint.
vestige	n. 残留，遗迹 Not a vestige of the ancient structure remains. 古建筑的残迹已荡然无存。
saturate	vt. 浸透 The continuous rain had saturated the soil.
scanty	a. 不足的 Details of his life are scanty. 关于他的生平，详细资料不多。
scoop	vt. 挖 n. 勺子 She scooped ice cream into their bowls. 她用勺把冰激凌舀到他们的碗里。 Scoop out the melon flesh. 用勺把瓜瓤挖出来。
salvage	vt. 海上救援，抢救  I was able to salvage some data from the ruined computer file.
satiate	vt. 使满足 The dinner was enough to satiate the gourmets. 晚餐足以让这些美食家们大饱口福。
scorch	v. 烤焦，使褪色 I scorched my dress when I was ironing it. 我把自己的连衣裙熨焦了。
scout	v. 侦查  n.侦察员 We went ahead to scout out the lie of the land. 我们先走一步，去侦察地形。
scrutinize	v. 仔细检查 The statement was carefully scrutinized before publication. 声明在发表前经过仔细审查。
secluded	a. 隐蔽的  live a secluded life
sedate	a.宁静的 I live in a sedate little village in the Midlands. 我住在英格兰中部一个宁静的小村庄里。
serene	a. 平静的 He went to countryside for a serene life.
instantaneous	a. 瞬间的，即刻的 Death was instantaneous because both bullets hit the heart. 因为两颗子弹都击中了心脏,所以死亡是瞬间发生的。
instigate	vt. 鼓动 They were accused of instigating racial violence. 他们被控煽动种族暴力。
instill	vt. 灌输 She instilled in the children the virtues of good hard work. 她慢慢给孩子们灌输勤奋工作的品德。
insubordinate	a. 不服从的     If you're insubordinate, I shall put you under arrest.
insular	a. 海岛的 A continental climate is different from an insular one.    a. 狭隘的  the old image of the insular, xenophobic Brit. 孤立保守而又恐外的英国人的旧有形象
intensify	vt. 加强 intensify the defense
intercede	vi. ~ (with sb) (for/on behalf of sb)（为某人）说情；（向某人）求情  They interceded with the authorities on behalf of the detainees. 他们为被拘留者向当局求情。
intercept	vt. 中途拦截，阻止 Reporters intercepted him as he tried to leave the hotel. 他正要离开旅馆，记者们把他拦截住了。 intercept radio messages 拦截无线电报
interlude	n. 间隔，插曲 Apart from a brief interlude of peace, the war lasted nine years.
intimidate	vt. 恐吓 I don’t want to intimidate you, but very few people pass this exam.
intoxicate	vt. 使陶醉，使中毒  He was intoxicated by many awards he received and ceased his step toward the peak of his career. intoxicating scent 醉人的芳香
intrepid	a. 勇敢的  an intrepid explorer 勇敢的探险家
intricate	a. 错综复杂的，难懂的 intricately carved sculpture
intrude	v. 侵扰 I don’t mean to intrude, but you have a phone call.
be inured to sth	习惯于xxx
invaluable	a. 无价的
invoke	vt. 请求帮助，尤其指向神 She constantly invoked death for her relief and deliverance. 她常常想在死亡中寻求解脱。
irreconcilable	a. 相对立的，无法调和的 These old concepts are irreconcilable with modern life. 这些陈旧的观念与现代生活格格不入。
germinate	v. 发芽 The seed germinates. An idea began to germinate in her mind.
gibe	     vt. 讥笑 gibe at sth
gild	     vt. 镀金  Carve the names and gild them. 把姓名刻上去，再镀上金。
gist         n. 要旨  I missed the beginning of the lecture─can you give me the gist of what he said? 我未听到讲座的开头——给我讲讲他说话的要点好吗？
gleam	     v. 发微光 The moonlight gleamed on the water. 月光照在水面上泛起粼粼波光。 Her eyes gleamed in the dark. 她的眼睛在黑暗中闪烁。
glean	vt. 收集 At present we're gleaning information from all sources. 目前，我们正从各种渠道收集信息。
gleeful	a. 高兴的，幸灾乐祸的 a gleeful laugh 欢快的笑声
gloom	n. 黑暗，忧愁 They could provide a ray of hope amid the general business and economic gloom.
gracious	a. 有礼貌的 a gracious lady
grandeur	n. 庄严，伟大 the grandeur and natural beauty of South America
gratify	vt. 使满意 We should gratify a child's thirst for knowledge.
gratuitous	a. 无缘无故的。不需要的 There's too much crime and gratuitous violence on TV. 电视里充斥着犯罪和无端的暴力。
greasy	a. 多油脂的 keep away from greasy food
gregarious	a. 群居的 Snow geese are very gregarious birds. 雪雁是群居性鸟类。
grim	a. 冷酷的，严厉的 a grim face 沮丧的
grind	vt. 磨碎 to grind coffee/corn grinding mill 研磨机
grope	vi. 摸索 grope around in the dark until he found the light switch
grouchy	a. 脾气不好的，喜欢抱怨的 bad-tempered
grudge	n. 怨恨 He has a grudge against the world. 他对社会心存不满。
grumpy	a. 坏脾气的
gush	vi. 涌出  Blood gushed out from his deep cut.
repel	vt. 击退  to repel an attack/invasion/invader 击退进攻 / 入侵；驱逐入侵者 v. 排斥  Like poles repel, unlike poles attract. 同极相斥，异极相吸。
繁殖，复制品	n. reproduction
repulsive	a. 令人厌恶的，排斥的
resonance	n. 回声
retort	v. 反驳 She retorted upon me, saying I was to blame. 她反驳我，说我才应该受责备
retract	v. 收回 to retract an offer 撤销提议 retract my words 收回我的话
revelation	n.揭露 The company's financial problems followed the revelation of a major fraud scandal. 重大的欺诈丑闻被揭露之后，公司随之出现了财政问题 startling/sensational revelations about her private life 对她的私生活令人吃惊的 / 轰动性的揭露
revere	vt. 尊敬 Students revere the old professors

rigorous	a. 严厉的，严峻的 The trainings soldiers received were rigorous.
rudimentary	a. 根本的，低级的 take a rudimentary cooking class
rugged	a. 粗糙的，不平的 a rugged road
ruminate	v. 沉思 It is worthwhile to ruminate over his remarks. 他的话值得玩味。
rupture	n.破裂  the rupture in the pipe
be solicitous about sth.	对xx感到焦虑，渴望
sparsely	adv. 稀少的 The province is heavily forested and sparsely populated. 该省森林茂密，人烟稀少。

sporadic	a. 零星的 Fighting continued sporadically for 2 months.
sprawl	n. 扩展 v. 蔓延  He was sprawling in an armchair in front of the TV.  attempts to control the fast-growing urban sprawl

stagger	v. 使蹒跚 He staggered home, drunk.

stern	a. 严厉的 The police are planning sterner measures to combat crime. 警方正制订更严厉的措施打击犯罪活动。
stoically	adv. 坚韧地，坦然的 She put up with it all stoically. 她坚忍地承受这一切。
strain	n. 紧张 v. 拉紧  Don’t strain your eyes by reading in poor light.  Relations with Japan are under strain at present.
strenuous	a. 费力的 Avoid strenuous exercise immediately after a meal. 刚吃完饭避免剧烈运动。

stringent	a. （法律规则）严格的，（财政）迫切的
stuffy	a. 不通风的，不透气的 It was hot and stuffy in the classroom. 教室里很闷热。
submerge	vt. 淹没 Her submerged car was discovered in the river by police divers. 她被水淹没的汽车给警方的潜水员找到了。 The waters were rising about the rock and would soon submerge it.
subvert	vt. 颠覆，推翻 subvert the state 颠覆国家
successive	a. 连续的 Jackson was the winner for a second successive year. 杰克逊已经是连续第二年获胜了。
succinct	a. 简明的，简洁的 a succinct explanation 简明的解释
succumb	vi. 屈服 ~ (to sth) 抵挡不住疾病，诱惑 Don't succumb to the temptation to have just one cigarette. 不要经不住诱惑，只抽一支烟也不行。 A few years later, Katya succumbed to cancer in London. 几年后，卡佳因患癌症在伦敦病逝。
sullen	a. 闷闷不乐 Bob looked pale and sullen. 鲍勃脸色苍白，闷闷不乐。
supersede	vt. 替代  The use of robots will someday supersede manual labour.
surmount	vt. 克服  Mary surmounted the problems caused by her handicap and finished college.
进行人口普查	conduct censuses
aerial	n. 天线 aerial photo 航空照片
advent	n. 到来 The leap forward in communication made possible by the advent of the mobile phone. 手机的问世带来了通讯业的迅猛发展
绕地球轨道运行	orbit the earth
waxing and waning of the moon	月亮的盈亏
disseminate	v. 传播 disseminate information
inscribe	v. 题字 His name was inscribed on the trophy. 他的名字刻在奖杯上。 The trophy was inscribed with his name. 奖杯上刻着他的名字。
calibrate	v. 校准，测定
integral	a. 不可或缺的，完整的 Rituals and festivals form an integral part of every human society. 仪式与节日构成了任何人类社会不可缺少的一部分。
vicinity	n. 周围地区 There were a hundred or so hotels in the vicinity of the railway station. 在火车站附近大约有100家旅馆。
beacon	n. 灯标
fortuitous	a. 偶然的 The occurrence of such things is by no means fortuitous.
meteorological	a. 气象的 China’s meteorological agency 中国气象局
necessitate	vt. 使成为必要  His new job necessitated him getting up at 6.  Recent financial scandals have necessitated changes in parliamentary procedures.
marketable	a. 畅销的 a highly marketable product
refine	vt. 精炼，提纯，改善 refine oil, sugar
homogenise	v. 使均匀
mar	v. 破坏 That election was marred by massive cheating. 大范围的舞弊破坏了那次选举。 His last months in office were marred by failing health. 由于健康恶化，他最后几个月的公职工作受到了影响。
unaided	a.独立地，不受帮助的 his first unaided walk
glacial	a. 冰河的 glacial landscape
stock-raising	畜牧业
be unaccustomed to sth	不喜欢做某事
seesaw	n. 跷跷板 v. 动摇 The tokyo stock market seesawed up and down.
amplify	vt. 放大 amplify sounds
perish	v. 死亡 A family of 4 perished in the fire.
culminate in sth.	以。。告终 Months of hard work culminated in success.
staple	n. 装订 a. 主要的  Rice is the staple food of more than half of the world population.
offshore	a. 海上的 an offshore island 近海岛屿 a. 离岸的 The island offers a wide range of offshore banking facilities.
stem from	源于  All my problems stemmed from drinking.
fodder	n. 饲料
blight	v. 损害  His career has been blighted by injuries. 他的事业不断受到伤病的困扰。
foul	v. 犯规 a. 难闻的 foul air Middlesbrough's Jimmy Phillips was sent off for fouling Steve Tilson.
grimace at sb./sth.	对。。做鬼脸
cloakroom	n. 衣帽间
public sphere	n. 公共领域
trail	v. 拖拉，跟着别人后面疲惫地走
surge	v. 涌  The crowd surged forward.  Flood waters surged into their homes.  v. 猛涨  Share prices surged.
stumble	v. 绊倒 I stumbled over a rock. 我在石头上绊了一下。
nifty	a. 有技巧的    There's some nifty guitar work on his latest CD.

hailstone	n. 冰雹雹块
prodigy	n. 天才  a musical prodigy 音乐奇才
torrential rain	倾盆大雨
perpetuate	vt. 延续 This system perpetuated itself for several centuries. 这一制度维持了几个世纪。
thence	adv. 然后 He was promoted to manager, thence a partnership in the firm.

ambivalent	a. 矛盾的  She remained ambivalent about her marriage. 她对于自己的婚事仍然拿不定主意。

collate	vt. 整理文件，校对 collate the data
corporal	n. 下士，a. 身体上的 corporal punishment 体罚
paediatrics	n. 儿科学
vantage	n. 优势 From a concealed vantage point, he saw a car arrive. 他从一个隐蔽的有利位置望出去，看见一辆车开了过来。

outstrip	vt. 超过 Demand is outstripping supply.
unpalatable	a. 令人不快的，难吃的
delude sb. into sth.	哄骗某人做某事
inexorable	a. 不可阻挡的 the seemingly inexorable rise in unemployment 看似不可阻挡的失业增长 Urbanization is inexorable trend of economic development. The crisis is moving inexorably towards war.
新陈代谢率	metabolic rate
frugally	adv. 节约地 He lived very frugally.

lethargy	n. 无精打采  Symptoms include tiredness, paleness, and lethargy. 症状包括疲倦、脸色苍白、没有精神。
cardiovascular	a. 心血管的
equable	a. 平和的 He was a fine person to work with and he was very equable and a patient man.
let off steam	宣泄，放松精神
presumable	a. 可能的
homogeneity	n. 同质性 The government panicked into imposing a kind of cultural homogeneity. 政府在恐慌下开始强制实施某种文化统一政策。
counter-productive	a. 适得其反的，有反作用的
glaring	a. 明显的，显眼的 a glaring error/omission/inconsistency/injustice 明显的错误 / 疏漏 / 不一致 / 不公正
keep… in check	镇住xxx  A free and open news media often keep corruption in check.
offspring	n. 孩子。子女，后代 Jack is her only offspring. 杰克是她唯一的后代。
havoc	n. 灾害 Continuing strikes are beginning to play havoc with the national economy. 持续的罢工开始严重破坏国家经济。
avidly	adv. 渴求地 She reads avidly. 她如饥似渴地阅读。 He was intensely eager, indeed avid, for wealth. 他嗜财如命。
outlay	n. 必要的开支 ~ (on sth) The best equipment is costly but is well worth the outlay. 最好的设备花费大，但这种开支很值得。
be pushed to the wall	被逼到墙角
mutation	n. 突变，变异 genetic mutation
peril	n. 危险 The country's economy is now in grave peril. 现在，这个国家的经济陷入了严重危机。
indiscriminate	a. 任意而为的 be indiscriminate in one’s choice of friends 交友不慎 indiscriminate use of antibiotics 滥用抗生素 indiscriminate slaughter 无差别屠杀
disperse	v. 散开 The crowd dispersed quickly. 人群很快便散开了。    Police dispersed the protesters with tear gas. 警察用催泪弹驱散了抗议者。
nuisance	n. 讨厌的人，麻烦事 It's a nuisance having to go back tomorrow. 明天不得不回去，真烦人。
grove	n. 树林 a grove of birch trees 白桦树丛
forage	n. 饲料 v. 觅食 The cat forages for food.
clutch	n. 离合器，大群人 v. 抓紧 He’s won a whole clutch of awards. He clutched the child to him.
be infested with	充满 The prison is infested with rats.
caste	n. 种姓 the caste system 种姓制度
specimen	n. 样品，标品 the contaminated specimen

shrub	n. 灌木
marshy	a. 沼泽的 The land is low and marshy.
funnel	n. 漏斗
sift	v. 筛选 sift through evidence
pitfall	n. 陷阱 the potential pitfalls of buying a house 购买房屋可能遇到的圈套
preservative	n. 防腐剂
ethylene	n. 乙烯
propylene	n. 丙烯
be out of bounds to xx	禁止xx进入 be out of bounds to the public / foreign journalists
tram	n. 有轨电车
pest	n. 害虫
mite	n. 螨虫
scrunch up	蜷缩起来
rationale	n. 根本原因 The rationale for this is that xxx
counteract	vt. 抵抗，抵消，抵制 counteract high blood pressure 降压 Schools are taking action to counteract bullying.
revert to	恢复到

stretch	v. 使竭尽所能  I need a job that can stretch me.  我需要一份让我充分发挥能力的工作.
shun	vt. 避开
sinister	a. 邪恶的 a sinister smile
slacken	vt. 使松弛  He slackened the ropes slightly. 他把绳子稍稍放松一些。
slender	a. 苗条的  her slender figure 她苗条的身材 Australia held a slender 1–0 lead at half-time. 澳大利亚队在上半场终时以1:0的微弱优势领先。
slump	vi. 猛然落下  He slumped down to the floor in a faint.
smear	vt. 涂，弄脏 The children had smeared mud on the walls. 那几个孩子往墙上抹了泥巴。
smother	vt. 使窒息，闷死  smother sb. with a pillow
snag	n. 障碍 It is to be expected that an experiment will sometimes run into a snag at first. 开始试验时有时不很顺手，也是很自然的。
sneer	vt. 嘲笑 He sneered at people who liked pop music. 他嘲笑喜欢流行音乐的人。
snobbish	a. 势利的
sojourn	vi. 逗留 During his sojourn in Africa he learned much about native customs.
susceptible	a. 易受感染的 Infants are more susceptible to illness than other people.
tacit	a. 心照不宣的，默许的
tamper with sth.	篡改，有意破坏 Someone had obviously tampered with the brakes of my car. 显然有人鼓捣过我汽车的刹车。
tantalize	v. 逗引 The boy would come into the room and tantalize the dog with his feed. 那个男孩会到房间里拿狗食逗弄狗。
tardy	a. 迟到的 He wept for the loss of his mother and his tardy recognition of her affection. 他痛哭流涕，不仅因为失去了母亲，还因为这么晚才认识到了妈妈对他的爱。
tarnish	vt. 败坏名誉 The affair could tarnish the reputation of the prime minister. 这一事件可能有损首相的名誉。
taunt	n. vt. 嘲笑  They taunted tom into losing his temper
temperance	n. 自制力  His temperance couldn’t be counted on.
temperate	a. 适度的，有节制的
tenacious	a.  抓住不放的，顽强的 The tenacious applicant soon got the job.
tentative	a. 暂时的，试验性的  Tentative measures have been taken to settle these refugees.
tenuous	a. 脆弱的 a tenuous thread
tepid	a. 微温的 a jug of tepid water 一壶温水
vigilant	a. 警惕的 the vigilant bodyguard
vindicate	vt. 证明，证明无辜 New evidence emerged, vindicating him completely. 新证据出现了，证明他完全是无辜的。
vogue	n. 流行    ~ (for sth). xx的流行 Despite the vogue for so-called health teas, there is no evidence that they are any healthier. Black is in vogue again.
vulgar	a. 庸俗的，粗俗的 vulgar jokes
wade	v. 涉水，蹚水 Rescuers had to wade across a river to reach them. 救援者必须蹚过一条河才能靠近他们
wane	vi. 衰减  His influence began to wane after his death.
warrant	n. 许可证 a arrest warrant They had a warrant to search the house.
whim	n. 心血来潮  We bought the house on a whim.
wholesome	adj. 有益健康的 eat wholesome food
withstand	v. 抵挡，经受 The materials used have to be able to withstand high temperatures.
wrath	n. 暴怒 She falls in love and incurs the wrath of her father.
wry	a. 哭笑不得的 “At least we got one vote”, she said with a wry smile.
yearn for sth	v. 渴望 Many young people in the country yearn for city life.

zest	n. 浓烈的兴趣 ~ (for sth) He had a great zest for life. 他对生命有着极大的热情。
flamboyant	a. 华丽的，浮夸的 flamboyant clothes
flare	v. 闪耀 The match flared and went out.
flicker	vt. 闪烁 There was something strange about the flickering blue light 闪烁的蓝光有点怪异。
flounder v.	vi. 挣扎 At that time the industry was floundering. 那时这个行业举步维艰。 Three men were floundering about in the water. 有 3 个人在水中挣扎。 She was floundering around in the deep end of the swimming pool. 她在游泳池深水区挣扎着。
flout	vt. 公然蔑视 flout the law
foe	n. 敌人 The knight was murdered by his foes.
foment	vt. 煽动 They accused him of fomenting political unrest. 他们指控他煽动政治动乱。
foremost	a. 最初的，一流的 one of the foremost scientist in china
forfeit	vt. （因犯规）丧失 He was ordered to forfeit more than £1.5m in profits.  v. 主动放弃 He has forfeited a lucrative fee but feels his well-being is more important.
forlorn	a. 绝望的 She waited in the forlorn hope that he would one day come back to her.
formidable	a. 令人敬畏的 In debate he was a formidable opponent. 在辩论中他是位难应付的对手。
forsake	vt. 遗弃 forsake smoking habit
fort	n. 要塞 The rebels besieged the fort. 叛乱者包围了城堡。
fortitude	n. 坚韧，刚毅 He bore the pain with great fortitude. 他以极大的毅力忍受了痛苦。
fortress	n. 堡垒，要塞 a 13th-century fortress
fracture	v. 断裂，骨折 His leg fractured in two places. 他的一条腿有两处骨折。 [VN] She fell and fractured her skull. 她跌倒摔裂了颅骨。
ponder	vt. 考虑，沉思 ~ (about/on/over sth) She pondered over his words.
potent	a. 强有力的 potent speech
preach	vt. 说教，布道
precede	vt. 先于 A meeting will precede the conference.
precipitous	a. 陡峭的 precipitous cliffs 险峻的峭壁 a. 急剧的 The stock market's precipitous drop frightened foreign investors.
preclude	vt. 排除，防止 ~ sth | ~ sb from doing sth His religious beliefs precluded him/his serving in the army.
precursor	n. 先兆 Dark clouds are often treated as precursor of a storm.
predominantly	adv. 占主导地位地，显著地
preeminent	a. 卓越的 the pre-eminent political figure in the country
premise	vt. n. 前提 His reasoning is based on the premise that xxx.
allege	vt. 宣称，指控 He is alleged to have mistreated the prisoners. 他被指控虐待犯人。
allot	vt. 分配，指派 I completed the test within the time allotted. 我在限定的时间内完成了试验。 How much money has been allotted to us?  我们分到了多少拨款？
amenable	a. 有责任的，肯接受的 be amenable to the law.
amenity	n. 生活设施 Some houses still lack basic amenities such as bathrooms. The hotel amenities include health clubs, conference facilities, and banqueting rooms.

annihilate	vt. 消灭 annihilate poverty
anomaly	n. 异常，反常现象 the apparent anomaly that those who produced the wealth, the workers, were the poorest 创造财富的工人最贫穷这一明显不正常现象
antagonist	n. 敌手  The rebels were antagonists of the ruling party.
antiseptic	a. 杀菌的， n. 消毒剂 You’d better get some antiseptic for that cut.
nostalgia	n. 思乡，怀旧 She is filled with nostalgia for her own college days.
oblige	vt. 强迫 be obliged to do sth Parents are obliged by law to send their children to school.
oblivion	n. 忘却，遗忘 An unexpected victory saved him from political oblivion. 一次意外的胜利使得他在政治上不再默默无闻。 Most of his inventions have been consigned to oblivion. 他的大部分发明都湮没无闻了。  n. 沉睡，昏迷 He often drinks himself into oblivion. 他常常喝酒喝得不省人事。
obscene	a. 猥亵的，淫秽的 obscene gestures/language/books
onset	n. （不好的事情）开始  Most of the passes have been closed with the outset of winter.  冬天来临，大部分关口都被关闭了。
optimum	a. 最优的 the optimum use of resources 对资源的充分利用 Aim to do some physical activity three times a week for optimum health. 为了达到最佳的健康状况，要力争每周进行3次身体锻炼。
penchant	n. 倾向，爱好 ~ for sth She has a penchant for champagne. 她酷爱香槟酒。
perennial	a. 长久的 the perennial problem of water shortage 缺水这个老问题
permeate	vt. 液体或气体渗透，弥漫 The smell of leather permeated the room. 屋子里弥漫着皮革的气味。 rainwater permeating through the ground 渗入地下的雨水
perplex	vt. 迷惑 They were perplexed by her response. 她的答复令他们困惑不解。
persevere	vi. 坚持，不屈不挠   ~ (in sth/in doing sth)| ~ (with sth/sb) Despite a number of setbacks, they persevered in their attempts to fly around the world in a balloon. 虽屡遭挫折，他们仍不断尝试乘气球环游世界。 She persevered with her violin lessons. 她孜孜不倦地学习小提琴。
pertinent	adj. 相关的 Please keep your comments pertinent to the topic under discussion.
pervade	vt. 遍布，弥漫 The spicy smell pervaded the kitchen.
pervert v.	vt. 误用 Some scientific discoveries have been perverted to create weapons of destruction. 某些科学发明被滥用来生产毁灭性武器。 v. 腐蚀 Some people believe that television can pervert the minds of children. 有些人认为，电视能腐蚀儿童的心灵。
petrify	vt. 变为化石，使吓呆 I'm petrified of snakes. 我特别怕蛇。
picturesque	a. 如画的 City dwellers sometimes long for a picturesque and serene rural life.
pivot	n. 轴 West Africa was the pivot of the cocoa trade. 西非是可可豆贸易的中心。
pivotal	a. 关键的 He has established himself as a pivotal figure in US politics.
placate	vt. 安抚，和解 The concessions did little to placate the students.
placid	a. 温和的 a placid baby/horse 安静的婴儿；驯良的马 the placid waters of the lake 平静的湖水
plead to sb.	v. 恳求某人
mishap	n. 不幸，小事故 After a number of mishaps she did manage to get back to Germany. 经历了一番波折之后，
modulate	vt. 调整（声音） He carefully modulated his voice. 他小心地压低了声音。
moist	a. 湿润的，多雨的  warm moist air 温暖潮湿的空气
mollify	vt. 安抚 She managed to mollify the angry customer.
monarch	n. 君主
mortal	n. 凡人，a. 终有一死的  We are all mortal. 我们都总有一死。
motif	n. 主题
multitude	n. 群众，大量 ~ (of sth/sb)  a multitude of possibilities 众多的可能性 a multitude of birds 一大群鸟 xx surrounded by a noisy multitude xx被喧闹的人群围着

nimble	a. 灵敏的，机敏的 keep their minds nimble  Sabrina jumped nimbly out of the van.
apprehend	vt. 领会，理解，抓获 Police have not apprehended her killer.
aptitude	n. 才能 I have no music aptitude.
arable	a. 适于耕种的
ardent	a. 热情的 Jane’s ardent admirer sent her flowers everyday.
arduous	a. 险峻的，困难的 The task was more arduous than he had calculated. 这项任务比他所估计的要艰巨得多。
arid	a. 干旱的 Africa has an arid climate.
aristocrat	n. 贵族
armament	n. 兵力
aspire to sth./doing sth.	渴望xx，做xx
assiduous	a. 勤勉的 an assiduous student
assimilate	vt. 吸收 I haven’t quite assimilated the new rules so i sometimes violate them by mistake.
assuage	vt. 缓和 To assuage his wife’s grief, he took her on a tour of Europe.
astute	a. 机敏的 She was politically astute. 她很有政治头脑。
attire	n. 服装 formal evening attire 晚礼服
aura	n. 气质 She always has an aura of confidence. 她总是满有信心的样子。
auspicious	a. 幸运的，吉祥的 They chose an auspicious day to start the business.
autocrat	n. 独裁者
avaricious	a. 贪婪的 These people are avaricious and will do anything for money.
barren	a. 贫瘠的
barter	n. 易货

balk	vt. 逃避 Even biology undergraduates may balk at animal experiments. 面对动物实验，哪怕是生物专业的大学生也会畏缩。 v. 妨碍
ballot	n. 选票 The chairperson is chosen by secret ballot. 主席是通过无记名投票选举产生的。
balmy	a. 温和的 have a balmy climate
bandit	n. 强盗 On her way home, a masked bandit suddenly rushed to her and robbed her of all her possessions.
banish	vt. 驱逐出境，通常是被动  John was banished from England. 约翰被逐出了英格兰，到中国。
becoming	a. 合适的，相称的 It was not very becoming behaviour for a teacher. 这种举止与一个教师的身份不太相称。
befuddle	vt. 使昏乱  Problems that are befuddling them. 搞得他们晕头转向的问题。 xxx befuddled with drink. 醉得一塌糊涂。
beget	vt. 引起，产生 Economic tensions beget political ones. 经济紧张导致政治紧张。
begrudge	vt. 羡慕，嫉妒 begrudge sb. agh I certainly don't begrudge him the Nobel Prize. 我当然不会嫉妒他得了诺贝尔奖。
beguile	vi. 欺诈 ~ sb (into doing sth) She beguiled them into believing her version of events.
belittle	vt. 轻视  She felt her husband constantly belittled her achievements. 她觉得她的丈夫时常贬低她的成就。
belligerent	a. 好战的 belligerent statements the belligerent countries 参战各国

bequest	n. 遗产 He left a bequest to each of his grandchildren. 他给他的孙辈每人留下一笔遗产。
berate	vt. 痛骂 Marion berated Joe for the noise he made.  玛丽昂严厉斥责乔吵吵闹闹。
bereave	vt. 丧偶，丧失至亲 Journalists stayed away from the funeral out of consideration for the bereaved family. 出于对丧失亲人家属的考虑，新闻记者没有到葬礼现场。
besiege	vt. 围 The speaker was besieged with questions.
bestow	vt. 授予  ~ sth (on/upon sb) The Queen has bestowed a knighthood on him. 女王已经授予他爵士头衔。
bewilder	vt. 迷惑，把。。弄糊涂 She was totally bewildered by his sudden change of mood. 他的情绪突变搞得她全然不知所措。
bicker	vi. 争吵 The couple bickered over little things.
bigoted	a. 偏执的 bigoted views
bland	a. 清淡的 bland dishes, bland foods 寡淡/清淡的食物
bleak	a. 凄凉的 The future looks bleak for the fishing industry. 渔业前景暗淡。 a. 荒凉的 a bleak landscape
extort	vt. 勒索 A blackmailer extorted thousands of dollars from the millionaire.
extraneous	a. 无关的  We shall ignore factors extraneous to the problem.
extricate	vt. 救出，使解脱 Jane extricated herself from an unhappy relationship with her boyfriend.
exude	vt. 流出 exude sweat
exultant	a. 狂欢的 The exultant crowds were dancing in the streets.
fallible	a. 会犯错误的 All human beings are fallible. 人人都难免犯错误。
famish	vt. 使挨饿 Many famished in the countryside during the drought.
fatuous	a. 愚昧的 fatuous remarks

feign	vt. 假装 He survived the massacre by feigning death. 他装死才在大屠杀中死里逃生。
ferret	vt. 搜索 The detective finally ferreted out the criminal.
fervent	a. 白热的 A fervent supporter of the feminist movement.
fester	vt. 溃烂 The wound did not fester.
fetter	vt. 束缚 the fetters of social convention 社会习俗的约束
feud	n. 世仇 The feud between our families has lasted for generations.
fickle	a. 多变的 The weather here is notoriously fickle. 这里的天气出了名的变化无常。
figment	n. 凭空想象的事情 a figment of sb's imagination 凭空想象的事物 Is Santa real or just a figment of people's imagination?
fitful	a. 一阵阵的 Your fitful pacing is bothering me.
swamp	n. 沼泽
reclining	a. 倾斜的 a reclining chair
recline	v. 躺，倾斜 Air france seats can recline almost like beds. recline in lounge chairs
compelling	a. 引人入胜的  Jan Roberts weaves a compelling tale which traps a young woman in a world run by the Mafia.
marginally	adv. 略微地 Sales last year were marginally higher than in 1991.
esoteric	a.   只有内行才懂的 He has an esoteric collection of old toys and games.
稀有品种动物	rare breed animals
pollinate	vt. 昆虫给花授粉 pollinate the local plants

unblemished	a. 毫无瑕疵的 He had an unblemished reputation. 他的名声白璧无瑕。 her pale unblemished skin 她那白皙光洁的皮肤
coat v.	v. 给。。涂上一层 coat the fish with flour a second coat of paint
underside	n. 底部 the underside of the car
municipal = metropolitan	a. 城市的
stark	a. 鲜明对比的 in stark contrast to
dispersal	n. 分散，散播 the dispersal of the seeds
dementia	n. 痴呆
deteriorate	v. 恶化 The situation might deteriorate further.
be bound up with xxx	与xxx关系密切
conviction	n. 信念，定罪 She has 6 convictions for theft. He plans to appeal against his conviction. It’s our firm conviction that xxx. He says with conviction.
paramount	a. 至高无上的 Safety is paramount.

hypnotic	a. 有催眠作用的 hypnotic music
overwhelmingly	adv. 压倒性地，大多数地
gimmick	n. 花招，把戏 a promotional gimmick
affiliation	n. 服从，隶属 The group has no affiliation to any political party.  该团体不隶属于任何政党。
remuneration	n. 薪水 He received a generous remuneration for his services. $ 31,000 is a generous remuneration. 31,000美元的薪水很不错了
a lump sum	一次性付清 I will pay in a lump sum.
infirmity	n. 长期体弱，虚弱，生病 the infirmities of old age 老年体弱
artery	n. 动脉
yeast	n. 酵母 ferment the yeast
hamster	n. 仓鼠
rhesus	n. 猕猴，恒河猴
pancreatic	a. 胰腺的 pancreatic cancer
insulin	n. 胰岛素
blood glucose	n. 血糖
elixir	n. 灵丹妙药 the elixir of life/youth 长生不老 / 永葆青春药
tout	v. 吹捧 The product is touted as being completely natural. 该产品被吹嘘为是全天然的
periodical	n. 期刊
药企	pharmaceutical company
honoraria	n. 谢礼  A group of residents agreed to conduct the survey for a small honorarium.
emblazon	v. 装饰 baseball caps emblazoned with the team’s logo
grapple with	与。。搏斗  Passers-by grappled with the man after the attack. 袭击之后过路人便与这男人扭打起来。
recalcitrant	a. 不受规章的，难以控制的 The University suspended the most recalcitrant demonstraters. 这所大学把几个反抗性最强的示威者开除了。
refrain from doing	v. 抑制 Please refrain from smoking.  n. 重复的评价  xxx has become a common refrain.  xxx已经是耳熟能详的老调了。
assertiveness	n. 自信
dock	n. 码头
be spot on	完全正确，对极了
get hold of sth	好不容易得到，设法和xx联络
be an absolute sensation	轰动一时
tiles	n. 瓷砖
orchard	n. 果园
be descended from xxx	是xxx的后裔
sanctuary	n. 鸟兽保护区
vitality	n. 生命力，活力  xxx bring vitality to its economy.
matinee	n. 电影的日场
uncontested	a. 无人异议的 He was the uncontested star of the team.
abdomen	n. 腹部 He was suffering from pains in his abdomen.
aeronautical engineer	航空工程师
meteorite	n. 陨石
vertebrate	n. 脊椎动物
abrasion	n. 表层磨损 He has severe abrasions to his right cheek.
adorn	vt. 装饰 The wall is adorned with paintings.
agitation	n. 煽动，焦虑 Diane lit a cigarette, trying to mask her agitation.
alchemy	n. 炼金术，魔力
algae	n. 藻类
ambience	n. 氛围，环境 They tried to create the ambience of a French bistro.
antiquated	a. 过时的，旧的 Those antiquated laws should no longer be used.
aptly	adv. 适当地 This year’s forum is aptly titled xxx.
aquatic	a. 水生的 aquatic sports
archipelago	n. 群岛 Solomon archipelago 所罗门群岛
artisan	n. 工匠，手艺人 The artisan can cut stones into various shapes. 这工匠能把石头雕成各种形状。
rule of thumb	n. 经验法则
ascribe sth to sth	把xx归因于 He ascribed his failure to bad luck.
aspiring	a. 渴望的，有志向的 Many aspiring young artist are advised to learn by copying the masters.
auger	n. 螺丝钻（打孔用）

bolster	v. 加强 bolster one’s confidence bolster up the economy
burgeon	v. 激增 a burgeoning population 急剧增长的人口 n. 嫩芽

burrow	v. 挖洞 Earthworms burrow deep into the soil.
bustle	n. 喧嚣 v. 忙碌 She bustled around in the kitchen.
cannibalism	n. 食人
canopy	n. 天蓬
carbohydrate	n. 碳水化合物  You should cut down on fats and carbonhydrate.
carnivorous	a. 食肉的  Snakes are carnivorous.
cater for sb./sth.	适合 The class caters for all ability ranges.
cater to sb.	迎合 They only publish novels which cater to the mass market.
charter	n. 宪章 the United Nations Charter 联合国宪章
chateau	n. 法国的城堡
chisel	v. 凿 A name was chiseled into the stone.
chivalry	n. 骑士精神，礼貌
civility	n. 礼貌 show one’s civility 很有礼貌
collective	n. 集体 a. 共同的 a collective decision
condense	v. 压缩，凝结 The article is condensed into just 2 pages.
confederacy	n. 联邦
confinement	n. 监禁，坐月子 He was being held in solitary confinement in the jail. She is still in confinement.  她还在坐月子
conifer	n. 针叶树
conjecture	v. 推测 He conjectured that the population might double in ten years.
constrain	v. 限制，约束 Research has been constrained by a lack of funds.
constraint	n. 限制 We have to work with severe constraints of time and money.
convivial	a. 欢乐的 The atmosphere was quite convivial.
crater	n. 坑，火山口 Ash began to erupt from the crater.
credence	n. 真实性，信任 They gave no credence to the findings of the survey.
creditor	n. 债主
custodian	n. 监护人，看守人 museum’s custodian
cylinder	n. 圆柱
daunting	a. 令人生畏的 She has the daunting task of cooking for 20 people every day.
descendant	n. 后裔  Many of them are descendants of the original settlers. 他们中许多人都是早期移民的后裔。
deter sb from sth./doing sth	v. 制止 Arming the police doesn’t deter crime.
devastation	n. 毁灭，破坏 The bomb caused widespread devastation.
digression	n. 离题，脱轨 Talking about xx would be a digression from the main purpose of this meeting.
disintegrate	vt. 分解 The catastrophic disintegration of the aircraft after the explosion.  The plane disintegrated as it fell into the sea.  The Empire began to disintegrate in 1918.
disproportionate	a. 不成比例的 A disproportionate amount of time was devoted to one topic. 纠缠在同一个话题上的时间太长了。
disrepute	n. 坏名声 Such people bring our profession into disrepute. 这种人令我们这一行蒙羞。
distend	v. 使膨胀 The balloon was distended because of filling of hydrogen.
distinct vs distinctive	distinct 明显的 There was a distinct smell of gas. distinctive 独特的 clothes with a distinctive style
divergence	n. 分歧，发散 There is a divergence of opinion within the party.

dorsal	a. 背部的 dorsal fin   背鳍
edifice	n. 大厦，宏伟建筑  a magnificent edifice
elevation	n. 高度，海拔 The road climbs to an elevation of 1000 feet.
elliptical	a. 椭圆的
emboss A with B/ emboss B on A	vt. 浮雕  stationery embossed with the hotel’s name
engrave	vt. 雕刻   [often passive] ~ A (with B) | ~ B on A The silver cup was engraved with his name. 银杯上刻有他的名字。
enclose	v. 把xxx围起来  The building was enclosed with iron railings.  Please enclose your price list and all necessary docs.
enterprising	a. 有进取心的

expressive	a. 有表现力的 She has wonderfully expressive eyes. 她有一双极富表情的眼睛。 the expressive power of his music 他的音乐的表现力
extinct	a. 灭绝的 become extinct
facilitate	v. 促进 The new airport will facilitate the development of tourism.
faction	n. 派别，团体 It is time for opposing factions to unite and work towards a common goal.
faucet	n. 龙头  the hot/cold faucet turn a faucet on/off
fend	v. 照顾 fend for yourself 照顾自己 fend off sth 避开xxx fend off questions 躲开问题 The police officer fended off the blows.
fleeting	a. 转瞬即逝的 pay a fleeting visit to Paris.

folklore	n. 民俗 In Chinese folklore, the bat is an emblem of good fortune.
flux	n. 不断变化 Our society is in a state of flux.
folly	n. 愚蠢 Giving up a secure job seems to be the height of folly. 放弃一份安定的工作似乎愚蠢至极。
formation	n. 形成，队形  The team plays in a 4-2-2 formation.
orthodox	a. 正统的 an orthodox Jew 正统的犹太教徒
transmute	v. 使变化 transmute A into B It was once thought that lead could be transmuted into gold.

premiere	n. 首映 The movie will have its gadgets in July.
onslaught	n. 猛烈的攻击 The town survives the onslaught of tourists every summer.
hasty	a. 匆忙的 make a hasty decision
jagged	a. 锯齿状的
jostle	v. 推挤 People in the crowd were jostling for the best positions.
intravenous	a. 静脉内的 an intravenous injection 静脉注射 Premature babies have to be fed intravenously. 早产婴儿必须靠静脉输入喂养。
overfill	v. 装到溢出

decrepit	a. 破旧的，体衰的 a decrepit old man
excise	v. 切除 The surgeon excised the tumor.
rampant	a. 犯罪猖獗的 rampant inflation Unemployment is now rampant in most of Europe.
candid	a. 坦白的 Few businesses are flourishing in the present economic climate.
flourish	v. 繁荣 These plants flourish in a damp climate.
veil	n. 面纱
diverge	v. 道路分开，意见分歧  Opinions diverge greatly on this issue.  He diverged from established procedure.
opaque	a. 不透明的 opaque glass
whimsical	a. 异想天开的 Much of his writing has a whimsical quality. 他的大部分作品都很出奇
heterogeneous	a. 不同种类的  the heterogenous population of the US
lubricate	v. 润滑  lubricate machinery/skin lubricate the discussion with xxx 促使会谈顺畅进行
oppress	v. 压迫 People are often oppressed by the government.
airtight	a. 密闭的  store the cake in an airtight container
rummage	v. 翻 ransack She was rummaging around in her bag for her drugs.
foster	v. 促进 foster better relations within the community v. 抚养 foster over 60 children foster parents 养父母
finitude	n. 有限，界限 The paper is about human finitude. 那报告是有关于人类的有限性。
inalienable	a. 不可分割的，不可剥夺的 the inalienable right to decide your own future
pejorative	a. 轻蔑的 a pejorative term 一个贬义词
nausea	n. 恶心  I have a feeling of nausea.
obtrude	v. 打扰 Music obtruded upon his thoughts.  音乐打扰了他的思维。
forensic	a. 法庭的 forensic science/medicine 法医学
pliable	a. （思想）容易受影响的，易弯的
stagnant	a. 停滞的 revise the stagnant economy
insidious	a. 潜伏的，不知不觉加剧的 the insidious effects of xxx 潜在后果 The changes are insidious.
reprove	vt. 责骂rebuke He reproved her for rushing away.  他责备她不该匆匆离去。
interim	a. 暂时的，过渡的  interim measures/government Her new job does not start until May and she will continue in the old job in the interim.
inexplicable	a. 无法说明的 She inexplicably announced her retirement.
formulate	vt. 制订 formulate a plan 系统阐述 She has lots of ideas, but she has difficulty formulating them.
audible	a. 听得见的 It was barely audible.
deluge	n. 洪水 v. 淹 接连不断的xxx  a deluge of calls/complaints Houses were damaged in the deluge.
plantation	n. 种植园 a banana plantation
prolusion	n. 序言，开场白
abut	v. 邻接 His land abuts onto a road.
recede	v. 后退 Jack’s footsteps receded into the night.
waive	v. 放弃，免除 waive the right to appeal 放弃上诉
indulge	v. 沉溺于 indulge in a glass of wine indulge oneself with a hot bath indulge children 娇惯孩子
hedge	n. 树篱
arise	v. 发生 A crises has arisen.
compatriot	n. 同胞 He played against one of his compatriots in the semi final.
moribund	a. 垂死的 the moribund patient
rendezvous	n. 约会 I had almost decided to keep my rendezvous with Tony.
caricature	n. 讽刺漫画
negate	vt. 否定 negate the results of elections v. 使无效 Alcohol negates the effects of the drug.
hegemony	n. 霸权 american hegemony
delinquent	a. 有违法倾向的 a delinquent teenager a. 拖欠的 a delinquent loan 逾期未还的贷款
dispel	v. 驱散  His speech dispelled any fears about his health.
peripheral	a. 外围的 a peripheral device a. 附带的，次要的 peripheral activities 非核心业务
plagiarize	vt. 剽窃 He was accused of plagiarizing his colleague's results
internist	n. 内科医师
lousy	a. 劣等的 a lousy weekend 一个糟糕的周末 what lousy weather
grumble	v. 怨言，牢骚  She’s always grumbling to me about how badly she’s treated at work.
exhilaration	n. 高兴 The Facebook IPO scheduled for this week is generating a rush of exhilaration on Wall Street. Facebook定于本周IPO，令华尔街兴奋不已。
acclimatize	v. 使服水土 arrive two days early to acclimatize
tyrannical	a. 残暴的，专制的 one of the world’s most oppressive and tyrannical regimes
clan	n. 家族 She ranks as my junior in the clan. 她的辈分比我小。
flowering	n. 繁荣 the flowering of Canadian literature
slumber	v. 睡眠 She fell into a deep and peaceful slumber.
plush	a. 豪华的 a plush hotel
frantically	adv. 疯狂的 They worked frantically to finish on time.
holistic	a. 整体的 integral
idyllic	a. 田园诗的 a house set in idyllic surroundings
mutinous	a. 叛变的 The mutinous sailors took control of the ship.
ignominy	n. 耻辱 the ignominy of being made redundant 被裁员的耻辱
ultraviolet	a. 紫外线的
felicity	n. 幸福 domestic felicity 家庭幸福
overpass	n. 立交桥，天桥
legible	a. 清晰的 My handwriting isn’t very legible. 我的字不是那么好认
deplore	vt. 谴责，悲痛 I deplore and condemn the killing.
derivative	n. 派生物  Crack is a highly potent and addictive derivative of cocaine.
consent	n.v. 同意  Children under 16 can’t give consent to medical treatment. She was chose as leader by common consent.
backbone	n. 脊椎  The small business people are the economic backbone of the nation.
memorable	a. 值得纪念的，难忘的 a memorable experience
predicament	n. 困境 the club’s financial predicament
venomous	a. 有毒的，恶毒的 venomous snake 毒蛇
stitch	v. 缝  The cut will need to be stitched.
inculcate	vt. 教诲，反复灌输 inculcate a sense of responsibility in sb. inculcate sb. with a sense of responsibility
analogue	n. 相似物
withhold	v. 拒绝给xxx，阻止 withhold sth. from sb./sth. She was accused of withholding information.
evict	v. 驱逐 evict sb. from sth.  They were evicted from their apartment for not paying the rent.
loutish	a. 粗野无礼的 the loutish behaviors
antecedent	n. 先辈 a Frenchman with Irish antecedents 祖先为爱尔兰人的法国人
bewail	vt. 为xx痛哭 bewail one’s lost youth
elbow	n. 肘 He has fractured his elbow.
regurgitate	vt. 反胃
anemia	n. 贫血
ephemeral	a. 短暂的 The pleasures are ephemeral.  快乐是短暂的
insulation	n. 隔绝，绝缘，隔音材料 A wet suit provided excellent insulation.
vouch	v. 担保 vouch for sb./sth. 替xx担保，替xx作证  I was in bed with the flu, my wife can vouch for that.
attentive	a. 专心的
ruinous	a. 导致毁灭的
valorous	a. 勇敢的 a valiant soldier
odyssey	n. 艰苦的跋涉 a 16-hour odyssey
lovelorn	a. 失恋的，单相思的 He was acting like a lovelorn teenager.
statutory	a. 法定的 Broadcasting has had to be regulated statutorily.
platitude	n. 陈词滥调 His speech is full of platitude.
hawk	n. 鹰
industrious	a. 勤奋的 She was an industrious student.
captive	a. 被捕获的 captive animals
pullulate	vi. 迅速繁殖 The lagoon was pullulated with tropical fish. 那个咸水湖聚满了热带鱼。
maltreatment	n. 虐待 2000 Prisoners died as a result of maltreatment.
pastoral	a. 田园的 a pastoral scene
torrent	n. 急流
sumptuous	a. 奢侈的 a sumptuous meal
mend	vt. 修理 mend the bike mend the shoes
lampoon	vt. 讽刺 His cartoons mercilessly lampooned the politicians.
backfire	v. 招致反效果 Unfortunately the plan backfired. 计划产生了适得其反的效果。
lava	n. 岩浆 molten lava
rendition	n. 表演
convalesce	vi. 恢复健康 She is convalescing at home after her operation.
gobble	v. 狼吞虎咽 gobble the food gobble up most of holiday budget
ingenuity	n. 心灵手巧，聪明才智 Inspecting the nest may require some ingenuity. 探查鸟巢是需要些技巧的。
deflation	n. 通货紧缩 Deflation is beginning to take hold in the clothing industry.
deduct	vt. 扣除 The cost of your uniform will be deducted from your wages.  Ten points will be deducted for a wrong answer.
flippancy	n. 轻率 His flippancy makes it difficult to have a decent conversation with him.
bewitch	vt. 施魔法于xxx  He was completely bewitched by her beauty.
halt	v. 暂停 She walked towards him and then halted.
footnote	n. v. 做脚注
opulent	a. 华丽的 opulent lifestyle
detour	n. 弯路 It’s well worth making a detour to see the village.
pediatrician	n. 儿科医师
wagon	n. 四轮马车
hoodwink	vt. 欺骗 She was hoodwinked into buying a worthless necklace.
ruffle	v. 使沮丧 get ruffled
epic	n. 史诗，伟大事迹 The match was an epic.
composite	n. 合成物 a composite of xxx 是xxx的综合
fledgling	n. 初出茅庐的，幼鸟 a fledgling enterprise a fledgling skier
quell	v. 压制 quell the unrest 平息了叛乱
indecent	a. 下流的，不雅的 indecent conduct/photos下流的行为；淫秽的照片 That skirt of hers is positively indecent. 她的那条裙子太暴露了。
intact	a. 完整无缺的 Most of the house remains intact after 200 years.
militant	n. 斗士
foregoing	a. 上述的 forego v. 放弃 foregone a. 已预知的  Most voters believe the result is a foregone conclusion.
cocky	a. 骄傲自大的 Don't get cocky when you've achieved something.
convex	a. 凸出的 a convex lens 凸透镜
unabridged	a. 未删节的
flaunt	vt. 炫耀 flaunt his wealth
bump	n. 肿块 She was covered in bumps and bruises.
infuriate	vt. 使生气 It infuriates me that she was not found guilty. 令我大怒的是她获判无罪。
recapture	vt. 夺回  Troops soon recaptured the island.
palpable	a. 明显可知的 The tense between a and b is palpable.
tributary	n. 支流 tributary of huangpu river

brazen	a. 黄铜制的
utterance	n. 说话 the Queen’s public utterance
onwards	adv. 向前地  The pool is open from 7am onwards.
introspection	n. 内省 have moments of quiet introspection 抽时间静思自己
detergent	a. 净化的 n. 清洁剂 liquid detergent 洗衣液 Grease marks can be removed with liquid detergent.
inflate	v. 使膨胀 inflate your life jacket by pulling on the cord
enmity	n. 敌意 There is an historic enmity between them.
outright	a. 直率的 Why don’t you ask him outright if it’s true? She laughed outright.
incorruptible	a. 廉洁的 He was a sound businessman, reliable and incorruptible.
subliminal	a. 潜意识的  subliminal advertising 软广  have a subliminal influence on xx
inviolable	a. 不可侵犯的 inviolable territory
manifest	v. 显现  The symptoms of the disease manifested themselves 10 days later. a. 明显的 His nervousness was manifest to all those present.
convection	n. 对流
ferocity	n. 凶猛，残忍  The police were shocked by the ferocity of the attack.
endow	vt. 资助，捐赠 The ambassador endowed a 1 million program. be endowed with sth. 天生赋有  She was endowed with intelligence and wit.  她天资聪颖。
embankment	n. 堤岸 The embankment was destroyed by rush of waters.
bellow	vt. 怒吼，咆哮 They bellowed at her to stop.
megacity	大城市
antidote	n. 解毒剂 There is no known antidote to this poison.
rejuvenate	vt. 使年轻，恢复活力 The government pushed through schemes to rejuvenate the inner cities.
oscillate	v. 波动 The share price has oscillate between 5 and 10.
keystone	n. 主旨 Keeping inflation low is the keystone of their economic policy.
overt	a. 明显的 Although there is no overt hostility, black and white students do not mix much.
barbaric	a. 野蛮的 The barbaric treatment of animals has no place in any decent society.
override	v. 否决，不理会 The chairman overrode the committee’s objections and signed the agreement. 不顾反对，签署了协议。
incinerate	vt. 焚化 The cars caught fire on impact and some of the victims were incinerated.
contempt	n. 轻视 show a contempt for sth.
objectify	vt. 将xx物化  magazines that objectify women
fortnight	n. 14天 He’s had 3 accidents in the past fortnight.
kit	n. 成套工具 toolkit
prick	v. 刺 He pricked the balloon.
haze	n. 薄雾
添加剂	additive  food additives additive–free juice
envisage	vt. 展望  What level of profit do you envisage?
mast	n. 旗杆 Flags were flying at half mast on all public holidays.
miniature	n. 缩小的模型，缩影 Paris is France in miniature.
insatiable	a. 不知足的 have an insatiable appetite for sth. 对xxx有难以满足的好奇心
entwine	vt. 使缠住 Her destiny was entwined with his.
adultery	n. 婚外情 She is going to divorce him on the grounds of adultery.
inflammable	a. 易燃的 be highly inflammable 高度易燃
erect	v. 竖立 Police has to erect barriers to keep crowds back.
reminiscent	a. 令人怀旧的 The way he laughed was reminiscent of his father. 他笑的样子让人想起他的父亲。 reminisce v.  We spent a happy evening reminiscing about the past. reminiscence n. 回忆录  a reminiscence of a wartime childhood
testify	v. 证明，为xx作证 He testifies that he was at the theatre at the time of the murder.
hop	n. 单脚跳 Kids hopping over puddles.
surveillance	n. 监视 The police are keeping the suspects under constant surveillance.
far-reaching	a. 影响深远的 far-reaching consequences
adept	a. 老手 be adept at doing sth
precipitation	n. 降雨量 an increase in annual precipitation
sanction	v.批准，国际制裁 Trade sanctions were imposed against any country that refused to sign the agreement.
superficial	a. 肤浅的 The book shows a superficial understanding of the historical context. a superficial injury 浅表伤
paradox	n. 悖论，矛盾体 He was a paradox - a loner who loved to chat to strangers.
ostentation	n. 铺张浪费 an official crackdown on ostentation
intangible	a. 无形的 intangible benefits
jaywalk	v. 乱穿马路 fine xxx $10 for jaywalking
utter	v. 发出 She did not utter a word during lunch. a. 完全 That’s utter nonsense.
indisposition	n. 小病，不适宜 She is the kind of person who will keep to her bed for the slightest indisposition.
tramp	n. 流浪汉 A tramp came to the door and asked for food.
outcast	n. 被排斥者 People with the disease were often treated as social outcasts.
impede	vt. 阻碍  Work is impeded by severe weather.
censorship	n. 审查制度  The government imposed strict censorship of the media.
argumentation	n. 论证 legal argumentation
accord	v. 符合 These results accord closely with our predictions.
decouple	v. 使脱离，分离 It will decouple Europe from the US.
plunder	v. 抢劫 The troops crossed the country, plundering and looting as they went.
lucid	a. 明白的，清晰的 a lucid account of xx 对xxx清晰的表述
millennium	n. 一千年，千禧年
divine	a. 神的 a divine punishment
integrant	a. 部分的
guzzle	v. 狂饮 guzzle soft drinks
perjury	n. 伪证 commit perjury 作伪证
seduce	vt. 勾引 seduce sb.
clash	n. 冲突 Clashes broke out between police and demonstrators. v. 冲突 clash with sb.
biography	n. 传记 biography of Kennedy
stipulate	v. 规定 The delivery date is stipulated on the contract.
discourage	vt. 阻止 discourage sb from doing sth. The weather discouraged people from attending. Her parents tried to discourage her from being an actress.
appendicitis	n. 阑尾炎  an operation for acute appendicitis
immeasurable	a. 不可估量的 cause immeasurable harm
temporal	a. 世俗的，时间的
disavowal	n. 否认 disavowal of sth 对xx的否认
cohabitation	n. 同居 unmarried cohabitation
internecine	a. 两败俱伤的 internecine strife in the party
interdisciplinary	a. 各学科的 an interdisciplinary research
bequeath	v. 遗赠 bequeath sth to sb.  He bequeathed his entire estate to his daughter.
perquisite	n. 利益，特权 Politics was the perquisite of the upper class. CF: prerequisite 先决条件，先修课程
articulate	v. 清晰地发音，说清楚 She struggled to articulate her thoughts.
stifle	v. 使窒息 I was stifling in the airless room. Most of the victims were stifled by the fumes.  v. 扼杀 stifle creativity
incumbent	n. 现任者 the present incumbent of the White House
persecute	vt. 迫害 be persecuted for sth 因xxx遭受迫害
impasse	n. 僵局 Negotiations have reached an impasse.
reinvigorate	vt. 使重新振作 reinvigorate the economy I felt reinvigorated after a shower.
expel	v. 驱逐 She was expelled from school.
scant	v. 缩减 a. 不足的 pay scant attention to 对xx不关注
prowess	n. 专长 He’s always bragging about his prowess as a cricketer.
contention	n. 争论  The island has been a bone of contention between the two countries. n. 观点  I would reject that contention.
clement	a. 和蔼的  A clement judge reduced his sentence.
voracious	a. 狼吞虎咽的，对知识渴求的 John was a voracious book collector.
ritual	n. 仪式 religious rituals
overthrow	vt. 推翻  The government was overthrown in a military coup.
decapitate	vt. 斩首  His decapitated body was found floating in a river.
pamper	vt. 纵享  pamper oneself with xxx 尽情享用xx v. 纵容  The son was pampered and spoiled.
overstate	vt. 夸大 overstate the impact
acerbic	a. 尖刻的 an acerbic letter
magnitude	n. 广大，重要性 An operation of this magnitude is going to be difficult.  这种规模的手术会很难。
centennial	a. 一百年的  centennial development of xxx  xxx的百年发展历程
foreshadow	v. 预示  The disappointing sales figures foreshadow more redundancies.
outward	a. 外面的 Mark showed no outward signs of distress.
majesty	n. 最高权威 Her majesty 女王陛下 His majesty 国王陛下 Their majesty 国王和王后
formality	n. 规范 go through all the formalities necessary to get a gun license n. 流程 Let’s skip the formalities and get down to business. He already knows he has the job, so the interview is a mere formality.
translucent	a. 半透明的
courtship	n. 求爱 They married after a short courtship.
recuperate	v. 恢复 He’s still recuperating from his operation.
herbicide	n. 除草剂
distress	n. 悲伤 The article caused the actor considerable distress.
coercive	a. 强制的 coercive measures
rivalry	n. 竞争 political rivalries 政治对抗 sibling rivalry 兄弟姐妹的对抗
disengage	vi. 解开 disengage A from B They wished to disengage themselves from these policies. disengage the clutch 松开离合器 The rocket disengaged and fell into the sea.
geometric	a. 几何的
legality	n. 合法性 They challenged the legality of his claim in the court.
brainy	a. 有头脑的
libel	v.n. 文字诽谤 He sued the newspaper for libel.
impoverish	vt. 使贫困 The changes are likely to impoverish single parent families even further. The rural people have been impoverished by a collapsing economy. poor=impoverished
humdrum	a. 单调的
seep	vi. 漏出 Blood seeps through the bandages.
inhibit	v. 抑制 inhibit sb. from doing sth. A lack of oxygen may inhibit brain development in the unborn child.
baffle	vt. 使困惑 His behaviour baffles me.
quandary	n. 困惑 be in a quandary 进退两难
uplift	n. 提起 an uplift in sales a 20% uplift 20%的涨幅
puberty	n. 青春期 reach puberty
stigma	n. 丢脸的事 There is no longer any stigma attached to being divorced.
lenitive	n. 润泽的，缓解的
amphibious	a. 两栖的 amphibious vehicles
uphill	adv. 向上 We cycled uphill for over an hour.
resurrection	n. 复活 the resurrection of the dead resurrect v. 使复活
indict	v. 起诉，指控 be indicted for sth.  The senator was indicted for murder.
exuberant	a. 热情洋溢的  a picture painted in exuberant reds and yellows
prologue	n. 开场白 the prologue to this novel
abysmal	a. 恶劣的 abysmal food safety records
effluent	n. 流出物 The waste effluents from the factory was dumped into the river.
inversion	n. 倒置 an inversion of the truth
incline	n. 倾斜  incline sb to sth. 倾向于 Lack of money inclines many young people towards crime.
adolescent	n. 青少年 adolescent boys
calumny	n. 诽谤 the victims of calumny
enfranchise	vt. 给予xx政治权利 be enfranchised 获得选举权
wholesale	n. 批发 buy sth. at a wholesale price 以批发价格购买
deform	vt. 使变形 She was born with deformed hands.
portfolio	n. 部长职务 She resigned her portfolio. 她辞去了部长职务。
consolation	n. 安慰 The children were a great consolation to him when his wife died.
smudge	n. 污迹 a smudge of lipstick on a cup
altitude	n. 海拔 We are flying at an altitude of 6000 meters.
freak	n. 狂热者 a jazz freak
repression	n. 压制 government repression
overhaul	v. 翻修 The engine is completely overhauled. A radical overhaul of the tax system is necessary.
herald	v. 预示 The talks could herald a new era of peace.
delusion	n. 错觉 I was under the delusion that he intended to marry me.
giddy	v. 眼花 I felt giddy when I looked down from the top floor.
segregate	vt. 隔离 segregate A from B.  Smoking and non–smoking areas are segregated from each other.
gridlock	n.交通全面堵塞 traffic gridlock n. 僵局 be in gridlock 陷入僵局
incompetent	a. 不称职的 incompetent government
lunatic	n. 疯子
multistage	a. 多级的


psyche	n. 心智 the human psyche 人类的心灵
impotent	a. 无能为力的 Without the chairman’s support, the committee is impotent.
annulment	n. 废除 the annulment of the election
audacious	a. 大胆的 an audacious decision
repose	n. 休息 in repose 在休息中
brunt	n. 冲击 bear the brunt of xxx 受到xxx的冲击
subsidy	n. 补贴 subsidies to sb.
equity	n. 平等 cf: equality
disempower	vt. 使失去权力  Women have been disempowered throughout history.
abject	a. 凄惨的 They died in abject poverty. a. 卑鄙的 an abject liar
fauna	n. 动物群
lenient	a. （执法时）宽容的 a lenient sentence The judge was too lenient with him.
invariably	adv. 不变地 The acute infection of the brain is almost invariably fatal.
alienate	vt. 使疏远 His comments have alienated a lot of voters.
incendiary	a. 放火的 incendiary attack 火攻 incendiary devices 纵火装置
incite	vt. 引起，煽动 incite the racial hatred He incited the workforce to come out on strike. They were accused of inciting the crowd to violence.
averse	a. 厌恶的 averse to sth  He was averse to any change.  他反对任何改变
pompous	a. 华而不实的
far-afield	adv. 远方 Journalists came from as far afield as China.
simultaneous	a. 同时发生的 the simultaneous release of the book and the album The two guns fires almost simultaneously.
mastery	n. 精通 have a mastery of several languages
perspiration	n. 出汗 Her skin was damp with perspiration.
zenith	n. 顶点 His career is now at its zenith.
overdraft	n. 透支 exceed overdraft limit 超出透支限额
livelihood	n. 生计 a means of livelihood They depended on fishing for their livelihood.
gross	n. 总额
underpin	vt. 加固xx的基础 A sense of mission underpins everything he does.
bust	n. 胸围 What’s your bust measurement?
consulate	n. 领事馆 the British consulate

vernacular	n. 方言，白话文 paraphrase the ancient Chinese prose in vernacular language.
vacillate	vi. 犹豫不定 vacillate between A and B
glare	n. 刺眼的光  The sunglasses are designed to reduce glare.
acquiesce	v. 默许 acquiesce in the decision
obstinate	a. 倔强的 the obstinate man a. 难以去除的 the obstinate stain
feminine	a. 娇柔的 That dress makes you look very feminine.
booth	n. 小房间 a phone booth
larceny	n. 盗窃罪 The couple were charged with grand/petty larceny.  重大，轻微盗窃罪。
scrub	v. 用力擦洗 scrub the floor
flutter	v. 振翅 Flags fluttered in the breeze.
mutiny	n. 兵变 Discontent among the ship’s crew finally led to the outbreak of mutiny.
bail	n. 保释 put up bail for sb. 保释某人 She was released on $209 bail. Bail was set at $1 million. He committed crime when he was out on bail.
lengthy	a. 冗长的 a lengthy explanation
spasm	n. 痉挛，抽搐 a muscle spasm
sabotage	v. 人为破坏 The main electricity supply was sabotaged by the rebels.

perpetrate	vt. 做坏事 perpetrate the crime
disorientate	vt. 使迷茫 They we’re disorientated by the smoke and were firing blindly into it. Ex–soldiers can be disorientated by the transition to civilian life.

deluxe	a. 豪华的 deluxe edition 精装版
superfluous	a. 过剩的，多余的 blow off one’s superfluous energy My presence was superfluous.
propitious	a. 吉利的 a propitious time to xx 做xx的黄道吉日
paltry	a. 微不足道的 a paltry 1% return on the investment
quench	v. 熄灭 quench the flames
fumble	v. 摸索 She fumbled in her pocket for a handkerchief.
fuss	n. 大吵大闹 make a fuss about sth. 为xxx大吵大闹
mayhem	n. 极端混乱状态 There was absolute mayhem when everyone tried to get out at once.
judicious	a. 明智的 Our leader made a judicious decision for our company’s future.
noxious	a. 有害的 noxious fumes 有毒气体
bulletin	n. 新闻简报 morning news bulletin a bulletin on the President’s health
abscond	v. 潜逃 He absconded with the company funds.
subside	v. 下沉 Weak foundation caused the house to subside.

latent	a. 潜在的 latent disease These children have a huge reserve of latent talent.
magniloquent	a. 夸大的
facsimile	n. 复制本  a facsimile of the painting a facsimile machine 传真机
involuntary	a. 无意的 She gave an involuntary shudder.  她不由自主地抖了一下。
interrogatory	a. 质问的  an interrogatory stare 带有疑问的注视
collude	v. 串通 collude with sb. in doing sth./ to do sth They colluded with terrorists to overthrow the government.
beware	v. 小心 beware of sth Drivers have been warned to beware of icy roads.
hollow	a. 空洞的 a hollow ball
conduction	n. 热或电的传导 heat conduction
footage	n. 影片片段 The police replayed footage of the accident over and over again.
congruent	a. 全等的 congruent triangle
rankle	v. 痛心  His decision to sell the land still rankles with her.
潜伏期	incubation period
amiable	a. 亲切的
counterfeit	n. 赝品  counterfeit watches 冒牌手表 counterfeit currency 假币
preface	n. 序言
cascade	v. 瀑布流下 Water cascaded down the mountainside.
undermine	v. 损坏（权威或信心）  The crisis has undermined his position.
aberrant	a. 异常的 aberrant behavior
intrigue	v. 密谋 intrigue with sb. against B v. 激起xx的兴趣  You’ve really intrigued me.
overtake	v. 追上  It‘s dangerous to overtake on a bend.  弯道超车很危险 overtake a car 超车 Nuclear energy may overtake oil as the main fuel.
envision	vt. 想象 envision sth/doing sth They envision an equal society.
fervor	n. 激情 religious fervor
inlet	n. 入口 air inlet
misconception	n. 误解 It’s a popular misconception that xxx.
beloved	n. 爱人 a. 心爱的 beloved wife
coincide	vi. 同时发生  The exhibition coincides with the 50th anniversary of his death.
grave	a. 重大的  The consequences will be very grave if nothing is done.
ward	n. 守卫，病房  emergency ward 急救室
prescience	n. 预知 It was an act of prescience, much criticized at the time. 这是一种有先见的行为，在当时却遭到批评。
impending	a. （不好的事）即将发生 warnings of impending danger
rebuff	v. 断然拒绝 They rebuffed her request for help.
buckle	n. 皮带扣 v. 扣住 buckle up the belt
woeful	a. 悲伤的
abound	vi. 大量存在 Stories about his travels abound. The lakes abound with fish.
vicious	a. 邪恶的 a vicious criminal
slavish	a. 盲从的 a slavish follower of fashion
appease	v. 平息 appease discontented workers
junction	n. 连接 the junction of xx road and xx road
commemorate	vt. 庆祝，纪念 A movie is shown to commemorate the 30th anniversary of his death.
encase	vt. 包起 The reactor is encased in concrete and steel.
adversary	n. 对手 He crossed the finish line just half a second behind his adversary.
bulge	n.v. 膨胀 bulge with Her pockets were bulging with presents.  His eyes bulged. a bulging briefcase
sanitary	a. 卫生的 sanitary condition
scoff	v.n.嘲笑 scoff at xxx.  At first I scoffed at the notion.
interweave	vt. 使交织 The blue fabric was interwoven with red and gold thread. A is intricately interwoven with B.  A和B关系密切。
mould	n. 霉 There’s mould on the cheese.
ensue	v. 接着发生 The companies grew tenfold in the ensuing 10 years.
hoard	v. 囤积 hoard food and gasoline
fiscal	a. 财政的 fiscal policy
reciprocate	v. 回报 The passion for him was not reciprocated.
mirage	n. 海市蜃楼
embargo	n. 禁运 impose an embargo on sth
interdependent	a. 互相依赖的 We live in an increasingly interdependent world.

sprinkle	v. 洒 sprinkle A on B, sprinkle B with A She sprinkled sugar over the strawberries.
motive	n. 动机 motive for murder 杀人的动机
vengeance	n. 复仇 take vengeance on sb. 对某人进行报复
dismantle	vt. 拆开（机器） I have to dismantle the engine to repair it.
omnipotent	n. 全能者  Money is not omnipotent, but we can’t survive without money.
conciliate	v. 安慰，和解 conciliate the people The two countries showed a willingness to conciliate.
chapel	n. 小教堂
rebound	n. 回弹 The ball rebounded from the goalpost and Owen headed it in.
spherical	a. 球的
miscellaneous	a. 混杂的，各种各样的 miscellaneous expenses 零花钱
tertiary	a. 高等的，第三的 the tertiary sector 第三产业 tertiary education 高等教育
conquest	n. 征服 military conquest
wrangle	vi. 争论 wrangle with sb. over sth. They are still wrangling over the financial details.
insurgence	n. 起义
sterilize	v. 消毒，杀菌 Sulphur is used to sterilize equipment. 消毒设备
discredit	vt. 使丢脸  The government is discredited.  政府颜面扫地。 v. 使怀疑 These theories are now largely discredited.
immaculate	a. 整洁的 Her room was kept immaculate.
dispiriting	a. 令人沮丧的 a dispiriting experience
ruffian	n. 恶棍，歹徒
plateau	n. 高原 Qinghai–Xizang Plateau
tribute	n. 致敬  This song is tribute to xx.
ostrich	n. 鸵鸟
hallowed	a. 神圣的 hallowed ground
asylum	n. 政治避难 apply for asylum
albeit	conj. 虽然  He finally agreed, albeit reluctantly, to help us.
genesis	n. 起源 the genesis of the universe
gigantic	a. 巨人般的 a gigantic task
trespass	vi. 侵犯，擅自进入 trespass on sth. trespass on private land
utmost	n. 极限，最大限度  do one’s utmost to do 尽力做xxx Our resources are strained to the utmost.
imperil	vt. 使陷于危险  Your imperiled the lives of other road users by your driving.
estrange	vt. 使疏远 estranged wife 关系疏远的妻子 He became estranged from his family after the argument.
sluggish	a. 迟缓的 The economy remains sluggish.
hallucination	n. 幻觉 High temperatures can cause hallucination.
obnoxious	a. 可憎的 obnoxious behaviour/character
snatch	v. 一把抢走 She managed to snatch the gun from his hand.
abbey	n. 修道院 Westminster Abbey
trammel	n. 束缚
extradition	n. 引渡 extradition proceedings 引渡程序
bailout	n. 紧急财政援助 Greece has requested a bailout to cover all its financial needs for 2 years.
tenable	a. 站得住脚的 The argument is simply not tenable.
repent	v. 懊悔 repent of sth. She repented of what she had done.
impenetrable	a. 难以理解的 The jargon is impenetrable to an outsider.
bawl	v. 大喊 She bawled at him in front of everyone.
yearning	n. 渴望 a yearning for a quiet life/another child
dash	v. 飞奔 She dashed along the platform and jumped on the train.
sluttish	a. 懒惰的
infiltrate	v. 使悄悄进入 The CIA agents successfully infiltrated into the terrorist organizations.
olfaction	n. 嗅觉 olfaction system
trauma	n. 外伤，痛苦的经历 The patient suffered severe brain trauma.
invincible	a. 无敌的 The team seemed invincible.
fallacy	n. 错误的公知 It’s a fallacy to say that the camera never lies.
apportion	vt. 按比例分配 They apportioned the land among members of the family.
mulish	a. 顽固的，等于stubborn
holler	v. 呼叫 Don’t holler at me.
rebuke	v. 指责 be rebuked for sth. The company was publicly rebuked for having neglected safety.
xenophobic	a. 恐惧或憎恨外国人的  Today the Internet is flooded with Chinese public opinion obsessed with xenophobic thoughts.
oversee	vt. 监督  Judge A was appointed to oversee the disposition of funds.
inorganic	a. 无机的 inorganic chemistry
grin	v. 咧嘴笑 grin at sb.
proficient	n. 能手 a. 精通的 be proficient at/in sth She is proficient in several languages. He is proficient at his job
sleight of hand	花招 Last year’s profits were more the result of financial sleight of hand rather than genuine growth.
poignant	a. 令人伤心的
effecfual	a. 奏效的 an effectual remedy

overrule	vt. 用权力否决 overrule a decision  The verdict was overruled by the Supreme Court.

sedentary	a. 需要久坐的 Obesity and a sedentary lifestyle has been linked with an increased risk of heart disease.
infinitesimal	a. 极小的 infinitesimal traces of poison
bemoan	vt. 埋怨 universities bemoan their lack of funds.
filth	n. 污物 The floor was covered in grease and filth.
barge	n. 驳船 v. 撞  He barged in on us while we were having a meeting.
ingest	vt. 摄入  It is reported that the protein which Americans ingest every day is twice their need in fact.
monstrous	a. 巨大的 a monstrous wave

interplay	v. 相互作用 The interplay between politics and the environment.
anesthesia	n. 麻醉 local anesthesia 局部麻醉 general anesthesia 全麻
auxiliary	a. 辅助的 augment the army and auxiliary forces 扩充军队和辅助队伍
bolt	vt. 拴住 bolt the door The gate bolts on the inside.  门从里面闩
ominous	a. 不祥的  There was an ominous silence at the other end of phone.
egalitarian	a. 平等主义的 an egalitarian society
lapse	n. 小错 v. 流逝  A momentary lapse in the final set cost her the match.
hassle	n. 麻烦  Send them an email, it’s a lot less hassle than phoning.
prophet	n. 伊斯兰教的先知
upheaval	n. 动乱 political upheaval 政治动乱 the latest upheaval in the education system 最近教育制度上的变更
disparity	n. 不同 the wide disparity between rich and poor economic disparities
slouch	v. 懒散地站，坐 Sit up straight, don’t slouch!
bogus	a. 伪造的 bogus name 假名 These figures were bogus and totally inaccurate.  数字是捏造的，不可信的。
nascent	a. 新生的，不成熟的 the nascent chicks
irrespective	a. 不顾的，无关的 Everyone is treated equally, irrespective of race.
entrust	vt. 委托 entrust A to B He entrusted the task to his nephew.
ailment	n. 疾病（小病） The pharmacist can assist you with the treatment of common ailments.
plethora	n. 大量，过多  A plethora of new operators will be allowed to enter the markets.
gully	n. 沟  The bodies of the 3 climbers were located at the bottom of a steep gully.
injudicious	a. 欠考虑的 injudicious remarks.
maladroit	a. 笨拙的  His first interview with the press was rather maladroit.
bridle	n. 缰绳
ubiquitous	a. 无所不在的 Sugar is ubiquitous in the diet.
hallmark	n. 特点 The explosion bore all the hallmarks of a terrorist attack.
justifiable	a. 有理由的 justifiable defense 正当防卫
calibre	n. 枪的口径 a .22 calibre rifle n. 人的品质 He was impressed by the high calibre of applicants for the job.
guile	n. 狡诈 I love children’s innocence and lack of guile.  我喜欢孩子的天真无邪。
viable	a. 可行的 a viable option
rebut	vt. 反驳 rebut the charges 反驳了xxx的指控
emeritus	a. 荣誉退休的 emeritus professor of biology
abominable	a. 恶劣的 The president described the killings as an abominable crime.
trivialize	vt. 使xxx显得不重要 The business world continues to trivialize the world’s environmental problems.
dividend	n. 红利  The first quarter dividend has been increased by nearly 4%.
corpus	n. 文集，语料库
fallow	a. 休耕的 Farmers are now paid to let their land lie fallow.
falter	v. 犹豫 She walked up to the platform without faltering.
perdition	n. 灭亡 the perdition of a country
reclaim	vt. 恢复 reclaim citizenship reclaim income tax 退税
wayward	a. 任性的 a wayward child
momentary	a. 瞬间的 a momentary lapse of concentration
studious	a. 好学的 a studious young man
lax	a. 懒散的 a lax attitude to health and safety regulations
proponent	n. 拥护者 Halsey was identified as a leading proponent of xxx.
austere	a. 严厉的 My father was an austere man. a. 禁欲的 the monk’s austere way of life
myriad	n. 无数，各种各样的 Designs are available in a myriad of colors.
crusade	n. 十字军
incorporate	vt. 把。。合并，纳入  The new car will incorporate a number of major improvements.
snazzy	a. 时髦的 a snazzy tie
martial	a. 战争的，军事的 He was convicted at a court martial. 他在军事法庭上被判有罪。
astray	adv. 误入歧途地 He is led astray by older children.
subsidise	v. 资助 Germany started to subsidise private pension saving.
brisk	a. 轻快的 take a brisk walk
acoustic	a. 有关声音的 acoustic waves 声波
acuminous	a. 锐利的 aluminiums marketing insight
emotive	a. 引起强烈情感的 Capital punishment is a highly emotive issue.  死刑是极易引起激烈争论的问题
unfeigned	a. 真诚的 We are all filled with infringed admiration for her achievements.
callous	a. 无情的 a callous disregard for human life
huddle	n. 混乱  People stood around in huddles.  人们三五成群地到处聚集着。
shamble	v. 蹒跚，站不稳  Sick patients shambled along the hospital corridors.
chink	n. 裂缝 a chick in the wall
overshadow	vt. 使扫兴 News of the accident overshadowed the day’s event.
apron	n. 围裙
pinprick	n. 针刺，光点  a pinprick of light 一丝光线
tricycle	n. 三轮车
occupant	n. 车内乘坐者，房子居住者 The car was badly damaged but the occupants were unhurt. All bills will be paid by the previous occupant.
hydropower	n. 水力发电
turbine	n. 涡轮机
tenfold	adv. 十倍  The population increased tenfold.
quadruple	adv. 四倍 Sales have quadrupled in the past 5 years.
typeface	n. 字体（印刷用）
solemnly	adv. 郑重地 She solemnly promised not to say a word.
paraconsciously	adv. 潜意识地
trance	n. 催眠状态，恍惚  be in a trance 精神恍惚
placebo	n. 安慰剂（医学），试验药物用的无效对照剂
autocratic	a. 专制的 an autocratic government
notoriety	n. 坏名声 She achieved notoriety for her affair with the senator. He gained a certain notoriety as a gambler.
enervation	n. 衰弱
faecal	a. 排泄物的 One of the ways the parasite spreads is through faecal matter.
lochs	n. 湖 Loch Ness
borne	由xxx携带 waterborne disease
pasture	n. 牧场  The cows are grazing in the pasture.
interbreed	v. 使杂交繁殖
shovel	n. 铲子  The children took their pails and shovels to the beach.
soil deterioration	土壤恶化
unrivaled	a. 无与伦比的
unwind	v. 解开  unwind the ball of string 解开一团绳
frontier	n. 边界 cross the frontier
systematisation	n. 体系化
cumulative	a. 累计的  the cumulative effect of human activities on the world environment
preferential	a. 优先的 get preferential treatment 受到优待
hieroglyph	n. 象形字
pulley	n. 滑轮
magnify	v. 放大 The dry summer has magnified the problem of water shortages.
rig up	仓促建起 He managed to rig up a shelter for the night.
scaffold	n. 绞刑架，断头台，脚手架
artefact	n. 具有历史或文化价值的手工艺品
arche	n. 本源 water is arche
nitrate	n. 硝酸盐 Nitrates are used as preservatives in food manufacture.
hedgerow	n. 树篱
subsidies	n. 补贴 Subsidies to farmers will be phased out by next year.
prop up	支撑，维持 I had to use bricks to prop up my broken bed.
premium	n. 保险费，额外费用 Even if customers want solutions, most are not willing to pay a premium for them.
locality	n. 地点，周围地区 People living in the locality of the power station.
handrail	n. 楼梯等的扶手
wheelbarrow	n. 手推车，独轮车
scrutiny	n. 仔细检查 Her argument doesn’t really stand up to scrutiny. His private life came under media scrutiny.
chant	v. 反复呼喊 Several thousand people chanted and demonstrated outside the building.
fungus	真菌（单数）fungi 复数
hail v.	v. 赞扬xx为xx  Matt is hailed a hero for saving a young child from drowning. A has been hailed as the greatest American novelist of his generation.
bearing	n. 关系，影响 bearing on sth Recent events had no bearing on our decision.
bounty	n. 悬赏金，奖金 A bounty of 100 was put on Jack’s head. v. 慷慨赠与 We received a bounty from the government.
perch	n. 鲈鱼
subsistence	n. 生存 Many families are living below the level of subsistence.  He worked a 16-hour day for a subsistence wage.
rectify	vt. 纠正 rectify the situation rectify a fault 改正缺点
prorate	v. 按比例分配 The rent was prorated for the rest of the month.
传送带	conveyor belt
annexe	n. 附属建筑
sleek	a. 毛柔顺亮泽的 sleek black hair  a. 外形流畅的，造型优美的 the sleek body of the iphone
paddock	n. 围场
vigilance	n. 警惕 Constant vigilance is necessary to avoid accidents.
cluttered	a. 乱七八糟 a cluttered room
unsolicited	a. 自发的 make unsolicited approaches to do sth
motorway	n. 高速公路（英）
coke	n. 焦炭 Coke is an economical fuel but it leaves a lot of ash.
charcoal	n. 木炭 charcoal grilled steaks a charcoal drawing
ferry v.	v. 摆渡 He offered to ferry us across the river in his boat.
commission v.	v. 委托 She has been commissioned to write a new national anthem. v. 任命 commission sb. as sth. He was commissioned as a pilot officer.
tilt	v. 倾斜 Suddenly the boat tilted to one side. She tilted the mirror.
unimpeded	a. 无障碍的 an impeded view of the bay
weld	v. 焊接 weld A and B together weld A to B
daunt	v. 使胆怯 I felt daunted by the task.
foothills	n. 山麓丘陵
rucksack	n. 登山包
underlying	a. 潜在的 Unemployment maybe an underlying cause of the rising crime rate.
prevailing	a. 普遍的 The prevailing view seems to be that they will find her guilty.
disorient	v. 使迷失方向  I felt dizzy and disoriented.
stately	a. 庄严的 The building rose before him, tall and stately.
demolition	n. 拆毁 The project required the total demolition of the old bridge.
derelict	a. 被遗弃的 a derelict warehouse
房地产开发商	property developer
synchronised swimming	花样游泳
binocular	a. 双目的 binocular vision
amino	a. 氨基的 amino acid 氨基酸
spoilage	n. 食物的变质
run-down	adj. 破败的 run-down inner-city areas 破败不堪的市中心区
sludge	n. 污泥
excretion	n. 动物的排泄物
hue	n. 颜色
backdrop	n. 背景 Their love affair was played out against the backdrop of war. 他们在战争的背景下发生恋情。 The mountains provided a dramatic backdrop for our picnic.
outdo	vt. 超越;胜过; Sometimes small firms can outdo big business when it comes to customer care. 在顾客服务方面，有时小企业可能优于大企业。
a cog in the machine	大组织中的小齿轮，螺丝钉 Mr Lake was an important cog in the Republican campaign machine. 莱克先生是共和党竞选机器中一枚重要的螺丝钉。
attenuate	vt. (使)减弱 You could never eliminate risk, but preparation and training could attenuate it. 风险不可能完全消除，但可以通过防范和培训来降低。
parched	a. 焦干的 dry parched land 焦干的土地
terrestrial	a. 陆地的，陆栖的 terrestrial animals
contemporary	n.同代人 a. 同一时期的 He was contemporary with the dramatist Congreve. 他与剧作家康格里夫属于同一时代。
bedsit	n. 大一室
reverberation	n.回声=echo
autistic	adj. 患自闭症的 autistic children
put it all down to xx	把一切归因于 Of course, it would be silly to put this all down to Foxconn. 当然，将这一切都归因于富士康是一件愚蠢的事。
promulgate	vt. 颁布(新法律或体制) A new constitution was promulgated last month. 上个月颁布了一部新宪法。
thwart	v. 阻挠 The accounting firm deliberately destroyed documents to thwart government investigators. 会计事务所故意毁坏文件，阻挠政府调查工作。
dog	v. 长期折磨 He had been dogged by bad health all his life. 他一生多病，备受折磨。
diffraction	n. 衍射 the diffraction of light
refract	v. 折射 Light is refracted when passed through a prism. 光通过棱镜时产生折射。
rod	n. 杆 fishing rod 鱼竿 steel rod 钢筋
rumbling	n. 轰隆隆的声音 the rumblings of thunder 隆隆的雷声
intertwine	v. (使)缠绕在一起 a necklace of rubies intertwined with pearls 缠着珍珠的红宝石项链 Their destinies are intertwined. 他们的命运交织在一起。
unspoilt	a. 未受损害的 an unspoilt coral reef 未遭破坏的珊瑚礁 Further to the south are some of the island's loveliest unspoilt coves. 再往南走有一些这个岛上尚未被破坏的迷人小海湾。
裸泳	nude bathing
invective	n. 谩骂
polemic	n. 辩论
propound	vt. 尤其是科学家提出(主意或解释)供考虑 the theory of natural selection, first propounded by Charles Darwin 查尔斯∙达尔文首先提出的物竞天择理论
undercut	v. 以低于…的价格出售 We were able to undercut our European rivals by 5%. 我们能以低于我们的欧洲对手5%的价格出售。
ailing	a. 有病的，不景气的 She looked after her ailing father. 她照顾有病的父亲。 measures to help the ailing economy 改善经济不景气的措施
saline	n. 生理盐水
inveigle	v. 哄骗 ~ sb/yourself (into sth/into doing sth) He inveigled himself into her affections. 他骗取了她的爱。
indented	adj.锯齿状的 an indented coastline
down-to-earth	a. 务实的 Their ideas seem to be far more down to earth and sensible. 他们的想法貌似更务实也更合理。
radiator	n. 暖气片
in earnest	adv. 认真地 No one could tell whether he was in earnest or in jest. 谁都不知道他是认真的还是在开玩笑。 His job as England manager begins in earnest now his World Cup campaign is in motion. 现在世界杯足球赛的征战工作已经展开，而他作为英格兰队主教练的工作也真正开始了。
leukaemia	n.   白血病
isotope	n. 同位素
longitudinal	a. 纵向的 The plant's stem is marked with thin green longitudinal stripes. 这种植物的茎上长有绿色细长条纹。
tug of war	n. 拔河 The students are engaged in a tug of war. 学生们正在进行拔河比赛。
veracity	n. 真实性 They questioned the veracity of her story. 他们质疑她所述事情的真实性。
lay a.	a. 外行的 His book explains the theory for the lay public. 他的书为大众阐明了这个理论。
stroll around	闲逛 We can just stroll around and try a few places. 我们就到那里转转，去一些地方看看。
berth	n. 床铺位 Goldring booked a berth on the first boat he could. 戈德林订了他能订到的最早一班船的铺位。
inclusive	a. 包括的 be inclusive of = include The rent is inclusive of water and heating. 租金包括水费和暖气费。 The fully inclusive fare for the trip is £52. 这次旅行的全部费用是52英镑。
frayed clothing	磨破的衣服
contingency	n. 可能发生的情况 I need to examine all possible contingencies. 我需要审视一切可能出现的情况。 No dress code can cover all contingencies so employees must exert certain amount if judgement in their choice of clothing to wear to work.
tasteful	a. 有品位的 The bedroom was tastefully furnished. 这卧室布置得很雅致。
nominate an insurance beneficiary	指定一个保险受益人
sabbatical	n. 休假 He's on sabbatical.
one-off	a. 一次性的
mobilise	v. 动员，调配资源 They are behind because they cannot mobilise fast enough.  他们落后的原因在于无法足够快的调配资源。
reprieve	v.n. 缓刑  Fourteen people, waiting to be hanged for the murder of a former prime minister, have been reprieved. 因谋杀前首相而即将处以绞刑的14个人已获缓刑。
vandalism	n. 故意破坏公共财物罪 There's no vandalism, no graffiti, no rubbish left lying about. 没有损害公共财物的行为，也没有涂鸦和随处丢弃的垃圾。
tender  v.	v. 投标 tender for sth Local firms were invited to tender for the building contract. 当地的公司被邀请投标承包建筑工程。
testament	n. 有力证据 The best testament that the spirit of Uppark had not died.
德国猪肘	pork knuckle
stilted	a. 生硬的 sound stilted  We made stilted conversation for a few moments. 我们不自然地客套了几句。
pastime	n. 消遣 Eating out is the national pastime in France. 在法国，下馆子是全国人普遍的消遣活动。
卡丁车	go-kart
quarry	n. 采石厂
blot	n. 损坏形象的污点 a blot on the landscape 风景中丑的一块 a blot on the reputation of the architectural profession 败坏建筑业名声的污点
reed	n. 芦苇 reed beds 芦苇荡

pro forma	a. 形式上的 Many employers have a pro forma user agreement that pops up when employees connect to an email or network server via a personal device.
exquisite	a. 精美的
folklore	民俗
aspiring	a. 有志向的
reoffend	v. 再次犯罪
probation	n. 假释
jaywalk	乱穿马路
fund	资助
predominate	v. 垄断市场
motorway	高速公路
preservative	n. 防腐剂
additive	n. 添加剂
lethargy	n. 无精打采
